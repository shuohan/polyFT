polyFTAddPath;

%% set parameters

SHOW_MESH = false;
SHOW_RECON = true;

kSampleNumX = 2 ^ 5;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

%% get mesh

[elements, vertices, intensities] = getDualTetrahedronVolumeMesh();
[faces, gradients] = simplifyElements(elements, vertices, intensities);

if SHOW_MESH
    showMesh(faces, vertices);
end

%% polyFT

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
imageSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);

tic
FT = polyFT(faces, vertices, kSamples, 'Type', 'optimized', 'Parallel', false, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'Gradients', gradients);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamples) / 3);

%% reconstruct

if SHOW_RECON 
    I = cartesianRecon(FT, kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
    figure, showRecon(I, imageSamples, 0.2); 
end
