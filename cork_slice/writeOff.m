function writeOff(faces, vertices, filename)

fid = fopen(filename, 'w');
faceNum = size(faces, 1);
verNum = size(vertices, 1);

fprintf(fid, 'OFF\n');
fprintf(fid, '%d %d 0\n', verNum, faceNum);

for i = 1 : verNum
    fprintf(fid, '%f %f %f\n', vertices(i, :));
end
    
if iscell(faces)
    for i = 1 : faceNum
        faceVerNum = length(faces{i});
        string = repmat('%d ', [1, faceVerNum]);
        string = ['%d ', string(1 : end - 1), '\n'];
        fprintf(fid, string, faceVerNum, faces{i} - 1);
    end
else
    faceVerNum = size(faces, 2);
    string = repmat('%d ', [1, faceVerNum]);
    string = ['%d ', string(1 : end - 1), '\n'];
    for i = 1 : faceNum      
        fprintf(fid, string, faceVerNum, faces(i, :) - 1);
    end
end

fclose(fid);