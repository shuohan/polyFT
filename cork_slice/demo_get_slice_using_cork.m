% plane parameters
point = [0, 0, 0];
normal = [1, 1, 1];

thickness = 10;

% load mesh as .mat file
load ../utils_mesh_generation/brain_mesh.mat
% 1. center the mesh
newCenter = mean([min(vertices); max(vertices)]);
vertices = vertices - repmat(newCenter, [size(vertices, 1), 1]);
% 2. move the plane point to the intersection of the plane and the line
% through [0 0 0] and [0 0 0] + normal
% 1) normalize normal
normal = normal / norm(normal);
% 2) find the projection (dot product) from point on normal
% 3) new point is normal * projection
point = dot(normal, point) * normal;

% write the mesh to a .off file
fileName = 'brain_mesh.off';
% writeOff(faces, vertices, fileName);

% construct a cuboid to intersect the mesh. Save the cuboid as a .off file
% 1. build the cuboid around [0 0 0]
% 1). find the coordinate with the largest absolute value
% 2). +- this value * 2 for the width and length of this cuboid
tmp = max(abs([min(vertices), max(vertices)])) * 2;
x = tmp; 
y = tmp;
z = thickness / 2;
boxVertices = [ x, -y, -z;
                x,  y, -z;
               -x,  y, -z;
               -x, -y, -z;
                x, -y,  z;
                x,  y,  z;
               -x,  y,  z;
               -x, -y,  z];
boxFaces = [1, 2, 5; 2, 6, 5;
            2, 3, 7; 2, 7, 6;
            3, 4, 8; 3, 8, 7;
            1, 5, 8; 1, 8, 4;
            1, 3, 2; 1, 4, 3;
            5, 7, 8; 5, 6, 7];

% 2. rotate this cuboid
% 3. move this cuboid to the new point along normal
rot = vrrotvec2mat(vrrotvec(normal, [0 0 1]));
boxVertices = repmat(point, [size(boxVertices, 1), 1]) + (rot * boxVertices.').';

figure, 
trisurf(boxFaces, boxVertices(:, 1), boxVertices(:, 2), boxVertices(:, 3), ...
    'faceAlpha', 0.8, 'faceColor', [1 1 1] * 0.5);
hold on
trisurf(faces, vertices(:, 1), vertices(:, 2), vertices(:, 3), ...
    'faceAlpha', 0.8, 'faceColor', [1 1 1] * 0.5);
axis equal

%%
% write box
writeOff(boxFaces, boxVertices, 'box.off');
% use cork to get the slice
system(['./cork -isct ', fileName, ' box.off result.off']);
% read the result .off file into matlab.

[resultFaces, resultVertices] = readOff('result.off');

%%
figure, 
trisurf(resultFaces, resultVertices(:, 1), resultVertices(:, 2), resultVertices(:, 3), ...
    'faceAlpha', 0.8, 'faceColor', [1 1 1] * 0.5);
axis equal