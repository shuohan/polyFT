#!/usr/bin/env bash

git clone https://github.com/gilbo/cork.git
mv cork cork_contents
cd cork_contents
make
cd ..
ln -s cork_contents/bin/cork ./cork
