function [faces, vertices] = readOff(filename)

% modified from Tri Ngo's code

fid = fopen(filename,'r');
fscanf(fid, 'OFF\n');
nums = fscanf(fid, '%d %d 0\n', [2 1]);

vertices = zeros(nums(1), 3);
verNums = zeros(nums(2), 1);
faces = cell(nums(2), 1);

%read vertices
for v = 1 : nums(1) 
    vertices(v, :) = fscanf(fid, '%f %f %f\n', [1 3]);
end

%read faces
for f = 1 : nums(2)
    verNums(f) = fscanf(fid,'%d ',[1 1]); % number of vertices in this face
    string = repmat('%f ', [1, verNums(f)]);
    string = [string(1 : end - 1), '\n'];
    faces{f} = fscanf(fid, string, [1, verNums(f)]) + 1;
end

fclose(fid);

if sum(verNums == verNums(1)) == nums(2)
    faces = cell2mat(faces);
end
