function printTimingInfo(totalTime, faceNum, kPointNum)

fprintf('Total computation time (s): %.4f\n', totalTime);
fprintf('Time per face per kpoint (us): %.4f\n', ...
    1e6 * totalTime / faceNum / kPointNum);
