function im = griddingRecon(kSampleLocs, kspaceData, gridSize, Lx, Ly, Lz, ...
    dcf, overgridFactor, convolutionWidth, kernelLen)

% INPUTS
% - kSampleLocs: kspaceDataNum x 2 matrix, the locations of kspace samples.
%   The first and second columns are coordinates along kx and ky
%   directions, respectively
% - kspaceData: kspaceDataNum x 1 complex matrix. The value of each sample
%   at the corresponding location.
% - gridSize: the size of gridded FT along kx and ky directions.
% - Lx: the size of the object along x direction
% - Ly: the size of the object along y direction
% - Lz: the size of the object along z direction, or thickness
% - dcf: kspaceDataNum x 1 matrix, density compensation factors;
% - kernelLen: the length of the kernel look-up table
% 
% OUTPUT
% - im: iFFT imag, gridSize x gridSize matrix.
%
% NOTICE
% kernel:
%         * ------
%      *     * 
%     *   x   *  kernel width
%     |*     *
%     |   * ------
%     |   |
%  convolution width
%
% REFERENCES
% - Rasche, Volker, et al. "Resampling of data between arbitrary grids
%   using convolution interpolation." Medical Imaging, IEEE Transactions on
%   18.5 (1999): 385-392.
% - Jackson, John, et al. "Selection of a convolution function for Fourier
%   inversion using gridding [computerised tomography application]." Medical
%   Imaging, IEEE Transactions on 10.3 (1991): 473-478.
% - Beatty, Philip J., Dwight G. Nishimura, and John M. Pauly. "Rapid
%   gridding reconstruction with a minimal oversampling ratio." Medical
%   Imaging, IEEE Transactions on 24.6 (2005): 799-808.
%
% -------------------------------------------------------------------------
% * Modified from Brian Hargreaves and Philip Beatty's code               *
% * http://mrsrl.stanford.edu/~brian/gridding/                            *
% -------------------------------------------------------------------------

if nargin <= 7
    overgridFactor = 2;
end
if nargin <= 8
    convolutionWidth = 1.5;
end
if nargin <= 9
    kernelLen = 32;
end

% map k-space sample locations to [-0.5, 0.5]

kmin = min(kSampleLocs(:));
kmax = max(kSampleLocs(:));
kSampleLocs = (kSampleLocs - kmin) / (kmax - kmin) - 0.5;

kernelWidth = convolutionWidth * 2;
[kernel, beta] = calcKBKernel(kernelWidth, overgridFactor, kernelLen); % a
    % look-up table; use linear interpolation to get the value not in the
    % table

griddedFT = gridMex(kSampleLocs, kspaceData, dcf, gridSize * overgridFactor, ...
    kernel, convolutionWidth);

if ndims(griddedFT) == 2
    im = cartesianRecon(griddedFT, gridSize, gridSize, Lx, Ly, Lz);
elseif ndims(griddedFT) == 3
    im = cartesianRecon(griddedFT, gridSize, gridSize, gridSize, Lx, Ly, Lz);
end

im = deapodization(im, beta, kernelWidth);

% crop the image

center = floor(gridSize * overgridFactor / 2);
startIdx = center - floor(gridSize / 2) + 1;
endIdx = center + floor(gridSize / 2) + mod(gridSize, 2);

if ndims(im) == 2
    im = im(startIdx : endIdx, startIdx : endIdx);
elseif ndims(im) == 3
    im = im(startIdx : endIdx, startIdx : endIdx, startIdx : endIdx);
end

% -------------------------------------------------------------------------

function [kernel, beta] = calcKBKernel(kernelWidth, overgridFactor, kernelLen)

% DECRIPTION
% calculates Kaiser-Bessel kernel.
%
% REFERENCES
% - eq. 1: Beatty, Philip J., Dwight G. Nishimura, and John M. Pauly.
%   "Rapid gridding reconstruction with a minimal oversampling ratio."
%   Medical Imaging, IEEE Transactions on 24.6 (2005): 799-808.
% - eq. 2: Jackson, John, et al. "Selection of a convolution function for
%   Fourier inversion using gridding [computerised tomography application]."
%   Medical Imaging, IEEE Transactions on 10.3 (1991): 473-478.

if nargin <= 2
	kernelLen = 32;                   	  
end
if nargin <= 1
    overgridFactor = 2;
end
if kernelLen < 2
	kernelLen = 2;	
	disp('kernelLen must be at least 2; set kernelLen to 2.');
end

alpha = overgridFactor;
w = kernelWidth;	

beta = pi * sqrt(w ^ 2 / alpha ^ 2 * (alpha - 0.5) ^ 2 - 0.8); % eq. 1

k = linspace(0, w / 2, kernelLen);

tmp = beta * sqrt(1 - (2 *  k ./ w) .^ 2); % eq. 2 (1)
kernel = besseli(0, tmp) ./ w;             % eq. 2 (2)

kernel = kernel / max(kernel);
kernel = kernel(:);

% -------------------------------------------------------------------------

function im = deapodization(im, beta, kernelWidth)

% DESCRIPTION
% applies deapodization for gridding
%
% INPUTS
% - im: the iFFT image of the gridding result; imSize x imSize matrix, or 
% imSize x imSize x imSize matrix
% - beta: a parameter used in Kaiser-Bessel kernel.
% - kernelWidth: the width of kernel in grid unit
%
% OUTPUT
% - im: result image

W = kernelWidth;
G = size(im, 1); % imSize
x = (1 : G) - G / 2; % pixel locations

tmp = sqrt((pi * W * x / G) .^ 2 - beta ^ 2);
c = sinc(tmp / pi); % matlab sinc(x) = sin(x * pi) / (x * pi);

if ndims(im) == 2
    cx = repmat(c, [G, 1]);
    cy = repmat(c.', [1, G]);
    c = cx .* cy;
elseif ndims(im) == 3
    cx = repmat(c, [G, 1, G]);
    cy = repmat(c.', [1, G, G]);
    cz = repmat(permute(c, [1 3 2]), [G, G, 1]);
    c = cx .* cy .* cz;
end

c = c / max(c(:));
im = im ./ c;

