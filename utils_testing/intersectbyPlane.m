function [faces, vertices] = intersectbyPlane(plane, faces, vertices)

% DESCRIPTION
% uses a plane to intersect the given triangular surface mesh, keeps the
% part that are inside the plane (on the opposite direction of the normal
% of the plane), and generates a new mesh.
%
% INPUT VARIABLES
% - plane: struct, plane.normal: the normal; plane.origin: an arbitary
%   point on the plane
% - faces: faceNum x 4 matrix; faces(:, 4) == faces(:, 1) to enclose each
%   facet.
% - vertices: vertexNum x 3 matrix.

% use the projection of each vertex to the plane to determine which edge is
% intersected by the plane
[dis, outer, inner] = getVertexPositions(plane, vertices);
[edges, edgeToFace, faceToEdge] = getEdges(faces);

% get the faces that are intersected by the plane
intersectedEdgeIdx = getIntersectedEdgeIdx(edges, outer, inner);

intersectedEdges = edges(intersectedEdgeIdx, :);
[newAddedVertices, intersectedEdgeToVerTmp] = getNewVertices(vertices, ...
    intersectedEdges, dis);
intersectedEdgeToVerTmp = intersectedEdgeToVerTmp + size(vertices, 1);

% get the new faces on the plane 
if ~isempty(intersectedEdgeToVerTmp)
    vertices = [vertices; newAddedVertices];
    [faceOnPlaneVerIdx, constraint] = getOnPlaneFace(edgeToFace, faceToEdge, ...
        intersectedEdgeIdx, intersectedEdgeToVerTmp);
end

% get the new faces inside the plane resulting from the intersected old faces
intersectedEdgeToVer = zeros(size(edges, 1), 1);
intersectedEdgeToVer(intersectedEdgeIdx) = intersectedEdgeToVerTmp;
intersectedFacesIdx = unique(edgeToFace(intersectedEdgeIdx, :));
intersectedFaces = splitIntersectedFaces(faces(intersectedFacesIdx, :), ...
    intersectedEdgeToVer(faceToEdge(intersectedFacesIdx, :)), inner);
faces(intersectedFacesIdx, :) = [];

% clean up faces and vertices 
outer = [outer; false(size(newAddedVertices, 1), 1)];
faces = removeOutsider(faces, outer);
faces = [faces; intersectedFaces];
[faces, vertices, map] = cleanVertices(faces, vertices);

% triangular faces on the plane
if ~isempty(intersectedEdgeToVerTmp)
    
    faceOnPlaneVerIdx = unique(map(faceOnPlaneVerIdx), 'stable');
    faceOnPlaneVer = vertices(faceOnPlaneVerIdx, :);
    facesOnPlane = triangulateFaceOnPlane(faceOnPlaneVer, constraint, ...
        plane, faceOnPlaneVerIdx);
    faces = [faces; facesOnPlane];
    
end


function [dis, outer, inner] = getVertexPositions(plane, vertices)

% distance from each vertex to the plane
%
% - plane: struct with "point" (any point on the plane) and "normal"
% - vertices: verNum x 3 array. Each row is a vertex
%
% - dis: verNum x 1 array.
% - inner: verNum x 1 logical array. True for the inner side (on the
%   opposite direction of the plane normal)
% - outer: verNum x 1 logical array. True for the outer side
% - on: verNum x 1 logical array. True for on the plane

disTmp = vertices - repmat(plane.point, [size(vertices, 1), 1]);
dis = disTmp * (plane.normal).';

outer = dis > 0;
inner = ~outer;

dis = abs(dis);

function [edges, edgeToFace, faceToEdge] = getEdges(faces)

% - faces: faceNum x 1 cells
%
% - edges: all possible edges, edgeNum x 2 array, contains the vertex
%   indices. 
% - edgeToFace: edgeNum x 2 array, edgeToFace(n, :) is the face indices of 
%   edges(n, :) (each edge is shared by two faces)
% - faceToEdge: faceNum x 1 cells; faceToEdge{1} is the array of edge
%   indices of faces{1}.
% - verToEdge: verNum x 1 cells; verToEdge{1} is the array of edge indices
%   of vertices(1, :)

facesTmp = faces.';

edgesTmp1 = reshape(facesTmp(1 : end - 1, :), ...
    (size(facesTmp, 1) - 1) * size(facesTmp, 2), 1);
edgesTmp2 = reshape(facesTmp(2 : end, :), ...
    (size(facesTmp, 1) - 1) * size(facesTmp, 2), 1);
edgesTmp = [edgesTmp1, edgesTmp2];
edgesTmp = sort(edgesTmp, 2); 

[edges, ~, faceToEdgeTmp] = unique(edgesTmp, 'rows');

faceToEdge = reshape(faceToEdgeTmp, [size(faces, 2) - 1, size(faces, 1)]);
faceToEdge = faceToEdge.';

[~, edgeToFaceTmp] = sort(faceToEdgeTmp);
edgeToFaceTmp = floor((edgeToFaceTmp - 1) / (size(faces, 2) - 1)) + 1;
edgeToFace = reshape(edgeToFaceTmp, [2, length(edgeToFaceTmp) / 2]);
edgeToFace = edgeToFace.';

% [edgesTmp, verToEdgeTmp] = sort(edges(:));
% % verToEdgeTmp: verTeEdge(x) is the edge idx of vertex with id = "edgesTmp(x)"
% verToEdgeTmp = mod(verToEdgeTmp - 1, size(edges, 1)) + 1;
% edgeNumPerVerTmp = find([diff(edgesTmp); true]);
% edgeNumPerVer = [edgeNumPerVerTmp(1); edgeNumPerVerTmp(2 : end) ...
%     - edgeNumPerVerTmp(1 : end - 1)];
% verToEdge = mat2cell(verToEdgeTmp, edgeNumPerVer);


function idx = getIntersectedEdgeIdx(edges, outer, inner)

outerTmp = outer(edges); % outerTmp(x, :) the two ends of edge x are out
innerTmp = inner(edges);
idx = (sum(outerTmp, 2) == 1) & (sum(innerTmp, 2) == 1);


function [newAddedVertices, intersectedEdgeToVer] = getNewVertices(vertices, ...
    intersectedEdges, verToPlaneDis)

% - intersectedEdges: intersectedEdgeNum x 2; 
% - verToPlaneDis: the distances between each vertex to the plane
% - intersectedEdgeToVer: intersectedEdgeNum x 1; the index of the newly
%   added vertices

ratios = verToPlaneDis(intersectedEdges);
ratios1 = repmat(ratios(:, 1), [1, 3]);
ratios2 = repmat(ratios(:, 2), [1, 3]);
ratioSum = repmat(sum(ratios, 2), [1, 3]);
newAddedVertices = (vertices(intersectedEdges(:, 1), :) .* ratios2 ...
    + vertices(intersectedEdges(:, 2), :) .* ratios1) ...
    ./ ratioSum;
intersectedEdgeToVer = (1 : size(newAddedVertices, 1)).';


function [verInOrder, constraint] = getOnPlaneFace(edgeToFace, faceToEdge, ...
    intersectedEdgeIdx, intersectedEdgeToVer)

% - intersectedEdgeIdx: the logical array

% convert

intersectedEdgeIdxTmp = zeros(size(edgeToFace, 1), 1);
intersectedEdgeIdxTmp(intersectedEdgeIdx) = 1 : sum(intersectedEdgeIdx);
faceToEdge = intersectedEdgeIdxTmp(faceToEdge);

edgeToFace = edgeToFace(intersectedEdgeIdx, :);

visitedFace = false(1, size(faceToEdge, 1));
visitedEdge = false(1, size(edgeToFace, 1));

facesOnPlane = {};

while any(visitedEdge == 0)

    faceTmp = zeros(1, size(edgeToFace, 1));

    currentEdgeIdx = find(~visitedEdge, 1);
    over = false;
    
    for i = 1 : length(faceTmp)
        
        visitedEdge(currentEdgeIdx) = true;
        if ~visitedFace(edgeToFace(currentEdgeIdx, 1))
            currentFace = edgeToFace(currentEdgeIdx, 1);
        elseif ~visitedFace(edgeToFace(currentEdgeIdx, 2))
            currentFace = edgeToFace(currentEdgeIdx, 2);
        else
            over = true;
            break
        end 
        visitedFace(currentFace) = true;
        
        faceTmp(i) = intersectedEdgeToVer(currentEdgeIdx);
        nextEdgeIdxTmp = faceToEdge(currentFace, :);
        nextEdgeIdxTmp = nextEdgeIdxTmp(nextEdgeIdxTmp > 0);
        for j = 1 : length(nextEdgeIdxTmp)
            nextEdgeIdx = nextEdgeIdxTmp(j);
            if ~visitedEdge(nextEdgeIdx)
                break
            end
        end
        currentEdgeIdx = nextEdgeIdx;
    end
    
    if over
        facesOnPlane = [facesOnPlane, faceTmp(1 : i - 1)];
    else
        facesOnPlane = [facesOnPlane, faceTmp];
    end
    
end

verInOrder = cell2mat(facesOnPlane);

constraint = [];
for i = 1 : length(facesOnPlane)
    constraintTmp = [(1 : length(facesOnPlane{i})).', ...
        [(2 : length(facesOnPlane{i})).'; 1]];
    constraint = [constraint; constraintTmp + size(constraint, 1)];
end


function faces = splitIntersectedFaces(faces, intersectedEdgeToVer, inner)

% - intersectedEdgeToVer: the same size with faceToEdge, each row contains 
%   the intersection info of edges of a face; 
%   zero means corresponding edge is not intersected; a non-zero value is 
%   the index of the intersected vertex of that edge.
% - inner: the length is equal to the total vertex number, indicating if a
%   edge is on the inner side of the intersection plane 

% three different conditions. For a intersected triangular face:
% [1 0 1], [1 1 0], [0 1 1];
% 1 means the edge is intersected

itersectedEdgeLocs = intersectedEdgeToVer > 0;
case1Loc = itersectedEdgeLocs * [1 0 1].' == 2;
case2Loc = itersectedEdgeLocs * [1 1 0].' == 2;
case3Loc = itersectedEdgeLocs * [0 1 1].' == 2;

case1 = faces(case1Loc, :);
case2 = faces(case2Loc, :);
case3 = faces(case3Loc, :);

case1NewVer = intersectedEdgeToVer(case1Loc, :);
case2NewVer = intersectedEdgeToVer(case2Loc, :);
case3NewVer = intersectedEdgeToVer(case3Loc, :);

%   for [1 0 1], the vertices is: ver1, newVer1, ver2, ver3, newVer2, ver1
%   for [1 1 0], the vertices is: ver1, newVer1, ver2, newVer2, ver3, ver1
%   for [0 1 1], the vertices is: ver1, ver2, newVer2, ver3, newVer2, ver1

case1 = [case1(:, 1), case1NewVer(:, 1), case1(:, [2 3]), case1NewVer(:, 3)];
case2 = [case2(:, 1), case2NewVer(:, 1), case2(:, 2), case2NewVer(:, 2), case2(:, 3)];
case3 = [case3(:, [1 2]), case3NewVer(:, 2), case3(:, 3), case3NewVer(:, 3)];

% convert all three cases to [1 0 1]
%   case [1 1 0], face([3 4 5 1 2])
%   case [0 1 1], face([4 5 1 2 3])

case2 = case2(:, [3 4 5 1 2]);
case3 = case3(:, [4 5 1 2 3]);

newFaces = [case1; case2; case3];

% then test the side of ver1
%   if inner(ver1) == false, keep face([2 3 4]), face([2 4 5])
%   else, keep face([1 2 5])

triInnerLoc = inner(newFaces(:, 1));
quadInnerLoc = ~triInnerLoc;

triFaces = newFaces(triInnerLoc, [1 2 5]);
quadFaces = [newFaces(quadInnerLoc, [2 3 4]); newFaces(quadInnerLoc, [2 4 5])];

faces = [triFaces; quadFaces];
faces = [faces, faces(:, 1)];


function faces = removeOutsider(faces, outer)

% removes faces on the outside of the plane

locToKeep = ~any(outer(faces), 2);
faces = faces(locToKeep, :);


function [faces, vertices, map] = cleanVertices(faces, vertices)

% deletes faces that are acutally edges or points due to duplication of
% vertices

[vertices, ~, map] = unique(vertices, 'rows', 'stable');
map = map.';
faces = map(faces);
facesLoc = (faces(:, 1) ~= faces(:, 2)) ...
    & (faces(:, 1) ~= faces(:, 3)) ...
    & (faces(:, 2) ~= faces(:, 3));
faces = faces(facesLoc, :);


function faces = triangulateFaceOnPlane(vertices, constraint, plane, ...
    vertexIdx)

% rotate the intersection plane to xy plane to perform 2d triangulation
rot = vrrotvec2mat(vrrotvec(plane.normal, [0 0 1]));
vertices = (rot * (vertices.')).';

DT = delaunayTriangulation(vertices(:, 1 : 2), constraint);
IO = isInterior(DT);
DT = DT(IO, :);
faces = vertexIdx(DT);
faces = [faces, faces(:, 1)];

% fix the orientation of new faces
edgesTmp1 = vertices(DT(:, 2), :) - vertices(DT(:, 1), :);
edgesTmp2 = vertices(DT(:, 3), :) - vertices(DT(:, 2), :);
normalsTmp = cross(edgesTmp1, edgesTmp2, 2);
dirTmp = normalsTmp * (plane.normal).' < 0;
faces(dirTmp, :) = faces(dirTmp, end : -1 : 1);   
