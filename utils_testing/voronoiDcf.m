function dcf = voronoiDcf(kspaceLocs)

% DESCRIPTION
% use voronoi diagram to calculate density compensation factors for
% gridding
%
% INPUT
% - kspaceLocs: sampleNum x 2 matrix. The first row is the coordinations
%   along kx direction; the second row is the coordinations along ky
%   direction.
% 
% OUTPUT
% - dcf: sampleNum x 1 array, density compensation factors
%
% REFERENCE
% Rasche, Volker, et al. "Resampling of data between arbitrary grids using
% convolution interpolation." Medical Imaging, IEEE Transactions on 18.5
% (1999): 385-392.

center = mean(kspaceLocs);
kspaceLocs = kspaceLocs - repmat(center, [size(kspaceLocs, 1), 1]);

[samples, ~, t2sIdx] = unique(kspaceLocs, 'rows', 'stable');
% s2tIdx: source to target indices, uniqueKLocs = kspaceLocs(s2tIdx, :);
% t2sIdx: target to source indices, kspaceLocs = uniqueKLocs(t2sIdx, :);

% outerHull is the set of the outmost samples of all samples
% innerSamples is the samples with outerHull removed
% innerHull is the set of the outmost samples of innerSamples
outerHullIdx = convhull(samples(:, 1), samples(:, 2));
outerHull = samples(outerHullIdx, :);

logicalOuterHullIdx= false(size(samples, 1), 1);
logicalOuterHullIdx(outerHullIdx) = true;

innerSamples = samples(~logicalOuterHullIdx, :);
innerHullIdx = convhull(innerSamples(:, 1), innerSamples(:, 2));
innerHull = innerSamples(innerHullIdx, :);

outerHullArea = polyarea(outerHull(:, 1), outerHull(:, 2));
innerHullArea = polyarea(innerHull(:, 1), innerHull(:, 2));

% to estimate the vorinoi cells of the outerHull samples, samples needs to
% be extrapolated, i.e. extra points surrounding outerHull (extraSamples) 
% needs to be calculated.
scaler = sqrt(outerHullArea / innerHullArea); % extrapolation factor
extraSamples = outerHull * scaler; % samples are centered to their mass
% center

% interpolate each edge of extraSamples (actually a convHull itself)
% to make sure reasonable voronoi cells can be calculated for outerHull
averageSpaceBetweenSamples = sqrt(outerHullArea / size(kspaceLocs, 1));
dxTmp = extraSamples(2 : end, 1) - extraSamples(1 : end - 1, 1);
dyTmp = extraSamples(2 : end, 2) - extraSamples(1 : end - 1, 2);
edgeLens = sqrt(dxTmp .^ 2 + dyTmp .^2);
interpolationNums = ceil(edgeLens / averageSpaceBetweenSamples) * 2;

interpolatedExtraSamples = zeros(sum(interpolationNums), 2);
startIdx = 1;

for i = 1 : size(extraSamples, 1) - 1

    xTmp =  linspace(extraSamples(i, 1), extraSamples(i + 1, 1), ...
        interpolationNums(i) + 1);
    xTmp = xTmp(1 : end - 1);
    yTmp =  linspace(extraSamples(i, 2), extraSamples(i + 1, 2), ...
        interpolationNums(i) + 1);
    yTmp = yTmp(1 : end - 1);
    tmp = [xTmp(:), yTmp(:)];

    endIdx = startIdx + interpolationNums(i) - 1;
    interpolatedExtraSamples(startIdx : endIdx, :) = tmp;
    startIdx = endIdx + 1;

end

[vertices, polyIdx] = voronoin([samples; interpolatedExtraSamples]);
dcf = zeros(size(samples, 1), 1);
% calculate areas of voronoi cells as density compensation factors
for i = 1 : size(samples, 1)
    dcf(i) = polyarea(vertices(polyIdx{i}, 1), vertices(polyIdx{i}, 2));
end

% handle duplicate samples
% duplicate samples should have averaged dcf

dcfDup = dcf(t2sIdx); 
[idx, sortIdx] = sort(t2sIdx);
dcfSorted = dcfDup(sortIdx);
[counts, ~] = hist(idx, unique(idx));
startIdx = 1;
for i = 1 : length(counts)
    for j = 0 : counts(i) - 1
        dcfSorted(startIdx + j) = dcfSorted(startIdx + j) / counts(i);
    end
    startIdx = startIdx + counts(i);
end
[~, inverseSortIdx] = sort(sortIdx);
dcf = dcfSorted(inverseSortIdx);

dcf = dcf / max(dcf);
