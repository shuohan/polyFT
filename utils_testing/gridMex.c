#include "grid.h"
#include "mex.h" 

/* function griddingMex
 * converts non-uniform sampling to uniform sampling
 *
 * SYNTAX:
 * griddedData = gridMex(dataPositions, nonUniformData,
 *     densityCompensation, gridSize, kernel, convolutionWidth)
 *
 * INPUTS:
 * - dataPositions: dataNum x 2 (2d acquisition) or dataNum x 3 (3d
 *   acquisition) matrix. The first column is the coordinates of kx; the
 *   second column is the coordinates of ky; the third column is the
 *   coordinates of kz.
 * - nonUniformData: dataNum x 1 complex matrix. Its order is the same with
 *   dataPositions.
 * - densityCompensation: dataNum x 1 matrix. Density compensation factors
 *   of each non-uniform sampled data point.
 * - gridSize: the size of griddedData. Assume x and y directions have the
 *   same size.
 * - kernel: kernelLen x 1 array
 * - convolutionWidth: the width of convolution area
 *
 * OUTPUT:
 * - griddedData: gridSize x gridSize matrix.
 */

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    #define dataPositionsMex prhs[0] 
    #define nonUniformDataMex prhs[1] 
    #define densityCompensationMex prhs[2]
    #define gridSizeMex prhs[3]
    #define kernelMex prhs[4]
    #define convolutionWidthMex prhs[5]
    #define griddedDataMex plhs[0]

    int i;
    double *kxLoc; // k-space x locations of nonUniformData
    double *kyLoc; // k-space y locations of nonUniformData
    double *kzLoc; // k-space z locations of nonUniformData
    double *nonUniformDataReal; // real part of nonUniformData
    double *nonUniformDataImag; // imaginary part of nonUniformData
    long nonUniformNum; // number of non uniform data
    double *densityCompensation; // density compensation factors
    double *griddedDataReal; // real part of griddedData
    double *griddedDataImag; // imaginary part of griddedData
    int gridSize;
    int *griddedDataDimensions;
    int griddedDataDimNum;
    double convolutionWidth;	
    double *kernel;	
    int kernelLen;
    
    if (nlhs != 1)
        mexErrMsgTxt("Wrong number of outputs");
    if (nrhs != 6)
        mexErrMsgTxt("Wrong number of inputs");
    if (mxGetN(nonUniformDataMex) != 1)
        mexErrMsgTxt("nonUniformData should be N x 1 matrix");
    if (mxGetN(dataPositionsMex) != 2 && mxGetN(dataPositionsMex) != 3)
        mexErrMsgTxt("dataPositions should be N x 2 matrix (2D acquisition) \
                or N x 3 matrix (3D acquisition)");
    if (mxGetN(densityCompensationMex) != 1)
        mexErrMsgTxt("densityCompensation should be N x 1 matrix");
    if (mxGetN(kernelMex) != 1)
        mexErrMsgTxt("kernel should be N x 1 matrix");
    if (mxGetM(densityCompensationMex) != mxGetM(dataPositionsMex)
            || mxGetM(nonUniformDataMex) != mxGetM(dataPositionsMex))
        mexErrMsgTxt("nonUniformData, densityCompensation, \
            and dataPositions should have the same number of data");
    if (mxGetM(gridSizeMex) != 1 || mxGetN(gridSizeMex) != 1)
        mexErrMsgTxt("gridSize should be a scaler");

    nonUniformNum = mxGetM(dataPositionsMex);
    nonUniformDataReal = mxGetPr(nonUniformDataMex);
    nonUniformDataImag = mxGetPi(nonUniformDataMex);
    densityCompensation = mxGetPr(densityCompensationMex);

    gridSize = (int)(*mxGetPr(gridSizeMex));

    kernel = mxGetPr(kernelMex);
    kernelLen = mxGetM(kernelMex);
    convolutionWidth = *mxGetPr(convolutionWidthMex);
    
    griddedDataDimNum = mxGetN(dataPositionsMex);
    griddedDataDimensions = malloc(griddedDataDimNum * sizeof(int));
    for (i = 0; i < griddedDataDimNum; i++){
        griddedDataDimensions[i] = gridSize;
    }
    griddedDataMex = mxCreateNumericArray(griddedDataDimNum, 
            griddedDataDimensions, mxDOUBLE_CLASS, mxCOMPLEX); 
    griddedDataReal = mxGetPr(griddedDataMex);
    griddedDataImag = mxGetPi(griddedDataMex);

    kxLoc = mxGetPr(dataPositionsMex);
    kyLoc = mxGetPr(dataPositionsMex) + nonUniformNum;
    if (griddedDataDimNum == 2)
    {
        grid2D(kxLoc, kyLoc, nonUniformDataReal, nonUniformDataImag, 
                nonUniformNum, densityCompensation, griddedDataReal, 
                griddedDataImag, gridSize, convolutionWidth, kernel, 
                kernelLen);
    }else
    {
        kzLoc = mxGetPr(dataPositionsMex) + 2 * nonUniformNum;
        grid3D(kxLoc, kyLoc, kzLoc, nonUniformDataReal, nonUniformDataImag, 
                nonUniformNum, densityCompensation, griddedDataReal, 
                griddedDataImag, gridSize, convolutionWidth, kernel, 
                kernelLen);
    }

}
