function maps = getCoils(imSize, coilNum)

% DESCRIPTION
% gets simulated ensitivity maps. The intensitiy distribution is contours
% of a hanning curve. Only support coil numbers: 4 8 16 32.
%
% INPUTS
% - imSize: the size of the target reconstructed image.
% - coilNum: integer from {4 8 16 32}
% 
% OUTPUTS
% - maps: result with the same size with imSize

if nargin == 1
    coilNum = 8;
end

assert(ismember(coilNum, [4, 8, 16, 32]));

width = max(imSize);
windowWidth = round(width * 1.7);
w = window(windowWidth);
angles = 0 : 2 * pi / coilNum : 2 * pi;
centers = ellipse([width, width], [windowWidth, windowWidth], ...
    angles(1 : end - 1));

maps = zeros(windowWidth * 2 + 1, windowWidth * 2 + 1, coilNum);
for i = 1 : coilNum
    maps(centers(i, 1) - round(windowWidth / 2) + 1 ...
        : centers(i, 1) - round(windowWidth / 2) + size(w, 1), ...
        centers(i, 2) - round(windowWidth / 2) + 1 ...
        : centers(i, 2) - round(windowWidth / 2) + size(w, 2), ...
        i) = w;
end

tmp1 = floor((size(maps, 1) - width) / 2);
maps = maps(tmp1 + 1: tmp1 + width, tmp1 + 1 : tmp1 + width, :);

sosMap = sos(maps);
scale = max(sosMap(:));
maps = maps / scale;

function pos = ellipse(axis, center, angles)
pos(:, 1) = center(1) + axis(1) / 2 * sin(angles);
pos(:, 2) = center(2) + axis(2) / 2 * cos(angles);
pos = round(pos);

function w = window(width)

center = repmat((width + 1) / 2, [1 2]);
[x, y] = meshgrid(1 : width, 1 : width);
x = x - center(1);
y = y - center(2);
ind = hanning(round(width * 1.2));
len = length(ind);
% ind = ind(round(len / 2) : round(len / 2) + round(width / 2));
ind = [ind(round(len / 2) : end); zeros(len, 1)];
% ind = [ind; ind(end) * ones(round(len / 2), 1)];
pos = ceil(sqrt(x .^ 2 + y .^ 2));
pos(pos == 0) = 1;
w = ind(pos);
