function showRecon(I, varargin)

% DESCRIPTION
% shows the 3D image reconstruction results
%
% INPUT VARIABLE
% - I: 3D image
%
% OPTIONS
% - varargin{1}: iPoints: the sampling points of the 3d image I; cell array
%   {iPointsX, iPointsY, iPointsZ}
% - varargin{2}: isovalue: the boundry value of isosurface

%% parse inputs

if length(varargin) < 1
    iPoints = {1 : size(I, 2), 1 : size(I, 1), 1 : size(I, 3)};
else
    iPoints = varargin{1};
end
if length(varargin) < 2
    isovalue = [];
else
    isovalue = varargin{2};
end

bottomMargin = 0.05;
leftMargin = 0.05;
space = 0.1;

width = (1 - 2 * leftMargin - space) / 2;
height = (1 - 2 * bottomMargin - space) / 2;
bottoms =[1 - bottomMargin - height, 1 - bottomMargin - height, ...
    bottomMargin, bottomMargin];
lefts = [leftMargin, 1 - leftMargin - width, ...
    leftMargin, 1 - leftMargin - width,];

%% 3d volume plotting

axes('Position', [lefts(1), bottoms(1), width, height])
isosurface(iPoints{1}, iPoints{2}, iPoints{3}, I, isovalue)

axis equal
axis tight
grid on
axis vis3d;
view(3)
set(gca, 'cameraviewangle', 10);
camlight headlight;
lighting phong
    
x = xlim; 
y = ylim; 
z = zlim;

x1 = min([x(1), y(1), z(1)]);
x2 = max([x(2), y(2), z(2)]);

xlim([x1,x2]); 
ylim([x1,x2]); 
zlim([x1,x2]);

xlabel('x');
ylabel('y');
zlabel('z')

% title('isosurface', 'fontweight', 'normal');

%% sections plotting

axes('Position', [lefts(2), bottoms(2), width, height])
imshow(squeeze(I(end : -1 : 1, floor(end/2), end : -1 : 1)).');
title('central y-z plane', 'fontweight', 'normal');
colorbar;

axes('Position', [lefts(3), bottoms(3), width, height])
imshow(squeeze(I(floor(end/2), :, end : -1 : 1)).');
title('central x-z plane', 'fontweight', 'normal');
colorbar;

axes('Position', [lefts(4), bottoms(4), width, height])
imshow(squeeze(I(end : -1 : 1, :, floor(end/2))));
title('central x-y plane', 'fontweight', 'normal');
colorbar;
