function I = backProjectionRecon(FT, Lx, Lz)

% DESCRIPTION
% reconstructs stack of stars sampling or 2D radial sampling.
% 
% INPUT VARIABLES
% - FT: k-space, sampleNum x projNum x sliceNum matrix
% - Lx: FOV along x direction
% - Lz: FOV along z direction
%
% OUTPUT VARIABLE
% - I: sampleNum x sampleNum (x sliceNum) matrix

[sampleNum, projNum, sliceNum] = size(FT);

factor = sampleNum * sampleNum * sliceNum / (Lx *  Lx * Lz);

I = complex(zeros([size(FT, 1), size(FT, 1), size(FT, 3)]));

FTtmp = fftshift(ifft(ifftshift(FT, 3), [], 3), 3);
FTtmp = fftshift(ifft(ifftshift(FTtmp, 1), [], 1), 1);

for i = 1 : size(FT, 3)
    I(:, :, i) = iradon(abs(FTtmp(:, :, i)), [], ...
        'linear', 'Ram-Lak', size(FT, 1));
end

I = I * factor;
