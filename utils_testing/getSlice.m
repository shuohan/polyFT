function [faces, vertices] = getSlice(faces, vertices, ...
    sliceNormal, origin, thickness)

% DESCRIPTION
% gets a slice (section) of the triangular surface mesh, or ideal slice
% selection in MRI. The result is a new triangular surface mesh with two
% parallel planes.
%
% INPUT VARIABLES
% - faces: faceNum x 4 matrix; faces(:, 4) == faces(:, 1) to enclose each
%   triangular
% - vertices: vertexNum x 3 matrix 
% - sliceNormal: the normal of (the direction vector orthogonal to) the
%   slice plane
% - origin: an arbitary point on the middle plane of the slice
% - thichness: the same unit with the mesh size
% 
% OUTPUT VARIABLES
% - faces: faces of the selected slice
% - vertices: vertices of the selected slice

plane.point = origin;
shift = sliceNormal ./ norm(sliceNormal) * thickness / 2; % half of the
    % thickness

% get one plane

plane.point = plane.point + shift;
plane.normal = sliceNormal;

[faces, vertices] = intersectbyPlane(plane, faces, vertices);

% get another plane

plane.point = plane.point - 2 * shift;
plane.normal = -sliceNormal;

[faces, vertices] = intersectbyPlane(plane, faces, vertices);
