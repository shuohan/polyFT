function [Lx, Ly, Lz] = getFOV(vertices, scale)

% DESCRIPTION
% gets FOV based on the range of vertices; the mesh is assumed to be around
% the origin (0, 0, 0) of image domain. FOV along x, y, z directions has
% the same size.
%
% INPUT VARIABLE
% - vertices: vertexNum x 3 matrix
%
% OPTION
% - scale: scaler; the FOV will be 100 x (scale - 1) percent larger than
%   the range of vertices
%
% OUTPUT VARIABLES
% - Lx, Ly, Lz: FOV along x, y, z directions, respectively.

if nargin == 1
    scale = 1.5;
end

Lx = scale * (2 * max(abs(vertices(:))));   
Ly = Lx;
Lz = Ly;
