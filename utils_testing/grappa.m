function kdata = grappa(kdata, calib, mask, R, kernelSize)

% DESCRIPTION
% straightforward implementation of 2D GRAPPA
%
% INPUT VARIABLES:
% - kdata: k-space data. Positions of data to estimate are occupied by
%   zeros. sampleNum x phaseNum x coilNum matrix
% - calib: calibration data. sampleNum x acsNum x coilNum matrix
% - mask: logical array with the same size with kdata; true indicates
%   sampled (non-zeros) positions.
% - R: down sampling rate
% - kernelSize: scaler, should be an odd number
%
% OUTPUT VARIABLE
% - kdata: filled k-space.

[patterns, indices] = getKernels(mask, R, kernelSize);
patternNum = length(patterns);
[sampleNum, phaseNum, coilNum] = size(kdata);

for p = 1 : patternNum 
    pattern = patterns{p};
    center = (length(pattern) - 1) / 2;
    yidx = indices{p};
    tmp1 = (yidx - center) >= 0;
    tmp2 = (yidx + center) <= sampleNum;
    yidx = yidx(tmp1 & tmp2);
    [rowIdx, colIdx] = getSurroundingPoints(pattern, yidx, coilNum); 
    xidx = sub2ind([phaseNum, coilNum], rowIdx, colIdx);
    for s = 1 : sampleNum
        kdataTmp = squeeze(kdata(s, :, :));
        X = kdataTmp(xidx);
        for c = 1 : coilNum
            weights = calibrate(squeeze(calib(s, :, :)), pattern, c);                                              
            kdata(s, yidx, c) = X * weights;
        end
    end
end

function [patterns, indices] = getKernels(mask, R, kernelSize)

% assume the same patterns are shared by each frequency encoding line

% mask is a 1-by-sampleNum logical array; 1 indicates possitions to
% compensate

patterns = cell(1, R - 1);
indices = cell(1, R - 1);
idx = find(mask);
radius = (kernelSize - 1) / 2;
starts = idx - radius;
ends = idx + radius;
tmp = find(starts >= 1 & ends <= length(mask));
firsts = tmp(1 : R - 1);

for i = 1 : length(patterns)
    range = starts(firsts(i)) : ends(firsts(i));
    patterns{i} = ~mask(range);
    indices{i} = idx(1 : R - 1 : end);
end


function weights = calibrate(calib, pattern, coilID) 

% calib: phaseNum-by-coilNum matrix
% X W = Y, W = X \ Y
% W: the weights, w-by-1 vector
% X: the surrounding data, n-by-w matrix
% Y: the target data, n-by-1 vector

phaseNum = size(calib, 1);
coilNum = size(calib, 2);

patternCenter = (length(pattern) + 1) / 2;
yidx = (patternCenter : phaseNum - patternCenter + 1).';

[rowIdx, colIdx] = getSurroundingPoints(pattern, yidx, coilNum);
xidx = sub2ind(size(calib), rowIdx, colIdx);

X = calib(xidx);
Y = calib(yidx, coilID);
weights = X \ Y;


function [rowIdx, colIdx] = getSurroundingPoints(pattern, yidx, coilNum)

yidx = reshape(yidx, [length(yidx), 1]);

center = (length(pattern) + 1) / 2;

rowIdx = find(pattern);
rowIdx = reshape(rowIdx, [1, length(rowIdx)]);
rowIdx = repmat(rowIdx, [length(yidx), 1]) + ...
    repmat(yidx - center, [1, length(rowIdx)]); 

colIdx = repmat(1 : coilNum, [size(rowIdx, 2), 1]);
rowIdx = repmat(rowIdx, [1, coilNum]);
colIdx = repmat((colIdx(:)).', [size(rowIdx, 1), 1]);
