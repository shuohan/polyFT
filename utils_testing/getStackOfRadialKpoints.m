function kpoints = getStackOfRadialKpoints(sampleNum, projNum, stackHeight, ...
    Lx, Lz)

% DESCRIPTION
%   Generates k space points as a stack of radial projections.
%
% INPUT VARS
%   - sampleNum: the number of points of one projection 
%   - projNum: the number of projectons per slice
%   - stackHeight: the number of slices
%   - Lx: FOV x axis length                                                                                           
%   - Lz: FOV z axis length                                               
%
% OUTPUT VARS
%    ~ k: k space vectors    
%

angles = linspace(0, pi, projNum + 1);
angles = angles(1 : end - 1);

tmp = ((0 : sampleNum - 1) - floor(sampleNum/2)) * (1/Lx);
tmp = repmat(tmp.', [1, length(angles)]); 

kx = tmp .* repmat(cos(angles), [size(tmp, 1), 1]);
ky = tmp .* repmat(sin(angles), [size(tmp, 1), 1]);
kz = ((0:stackHeight-1) - floor(stackHeight/2)) * (1/Lz);
kz = permute(kz, [3 1 2]);

kpoints = zeros([size(kx), length(kz)]);
kpoints(:, :, :, 1) = repmat(kx, [1, 1, length(kz)]);
kpoints(:, :, :, 2) = repmat(ky, [1, 1, length(kz)]);
kpoints(:, :, :, 3) = repmat(kz, [size(kx), 1]);
