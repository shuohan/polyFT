function im = sos(im)

% DESCRIPTION
% sum of square

im = sqrt(sum(im .* conj(im), 3) / 3);
