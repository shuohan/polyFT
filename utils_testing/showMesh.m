function showMesh(faces, vertices, showNodes, colors)

% DESCRIPTION
% Shows the mesh according to faces and vertices.
%
% INPUT VARIABLES
% - faces: faceNum x edgeNum matrix, OR faceNum x 1 cell array with each
%   element containing a 1 x edgeNum array
% - vertices: vertexNum x 3 matrix
% OPTIONS
% - showNodes: true or false; true: show the vertex nodes. (default: true)
% - colors: length is equal to faceNum, the color for each face. (default:
%   [], the same color for each face)

if nargin <= 2
    showNodes = true;
end
if nargin <= 3
    colors = [];
end

alpha = 0.8;
color = [0.5 0.5 0.5];

if iscell(faces)
    faceLengths = cellfun(@(x) length(x), faces);
    uniqueLengths = unique(faceLengths);
    for i = reshape(uniqueLengths, [1, length(uniqueLengths)])
        idx = faceLengths == i;
        tmpFaces = cell2mat(faces(idx));
        if isempty(colors)
            plotMeshSameColor(tmpFaces, vertices, color, alpha);
        else
            plotMeshDifferentColors(tmpFaces, vertices, colors(idx), alpha);
        end
    end
else
    if isempty(colors)
        plotMeshSameColor(faces, vertices, color, alpha);
    else
        plotMeshDifferentColors(faces, vertices, colors, alpha);
    end
end

if showNodes
    hold on
    plot3(vertices(:, 1), vertices(:, 2), vertices(:, 3), 'o', ...
        'markeredgecolor', 'k',  'markerfacecolor', color, ...
        'markersize', 8, 'linestyle', 'none')
end

xx = xlim; yy = ylim; zz = zlim;
x1 = min([xx(1), yy(1), zz(1)]);
x2 = max([xx(2), yy(2), zz(2)]);

xlim([x1,x2]); ylim([x1,x2]); zlim([x1,x2]);
xlabel('x');ylabel('y');zlabel('z')

grid on
axis vis3d;
axis equal
axis tight
view(3)
set(gca, 'cameraviewangle', 10);


function plotMeshSameColor(faces, vertices, color, alpha)
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3),...
    'edgecolor', 'k', 'facecolor', color, 'facealpha', alpha);


function plotMeshDifferentColors(faces, vertices, colors, alpha)
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), colors,...
    'facealpha', alpha);
colormap gray
