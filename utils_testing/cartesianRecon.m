function I = cartesianRecon(FT, varargin)

% DESCRIPTION
% reconstructs the 3d image from FT (uniformly sampled)
% 
% EXAMPLES
% 3D: I = cartesianRecon(FT, Nx, Ny, Nz, Lx, Ly, Lz);
% 2D: I = cartesianRecon(FT, Nx, Ny, Lx, Ly, thickness);
%
% INPUT VARIABLES
% - Nx: points number along kx axis                                     
% - Ny: points number along ky axis                                     
% - Nz: points number along kz axis                                     
% - Lx: FOV x axis length                                               
% - Ly: FOV y axis length                                               
% - Lz: FOV z axis length                                               

% deal with the coefficient different from inverse FFT
if length(varargin) == 6 % 3D
    [Nx, Ny, Nz, Lx, Ly, Lz] = varargin{:};
    factor = Nx * Ny * Nz / (Lx *  Ly * Lz); 
elseif length(varargin) == 5 %2D
    [Nx, Ny, Lx, Ly, thickness] = varargin{:};
    factor = Nx * Ny / (Lx *  Ly * thickness); 
else
    error('Wrong input variable number. It should be 5 (2D) or 6 (3D).');
end

FT = FT * factor;
I = fftshift(ifftn(ifftshift(FT)));
I = abs(I);
