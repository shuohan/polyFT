function samples = getImageSpaceSamples(varargin)
%
% DESCRIPTION
% Generates the grid of image pixel positions
%
% EXAMPLES
% 3D: samples = getImageSpaceSamples(Nx, Ny, Nz, Lx, Ly, Lz);
% 2D: samples = getImageSpaceSamples(Nx, Ny, Lx, Ly);
%
% INPUT VARIABLES
% - Nx: points number along kx axis                                     
% - Ny: points number along ky axis                                     
% - Nz: points number along kz axis                                     
% - Lx: FOV x axis length                                               
% - Ly: FOV y axis length                                               
% - Lz: FOV z axis length                                               
%
% OUTPUT VARIABLE
% - samples: 3D: 1 x 3 cell array; 2D: 1 x 2 cell array

if nargin == 6

    [Nx, Ny, Nz, Lx, Ly, Lz] = varargin{:};

    iPointsx = linspace(-Lx/2, Lx/2, Nx);
    iPointsy = linspace(-Ly/2, Ly/2, Ny);
    iPointsz = linspace(-Lz/2, Lz/2, Nz);
    samples = {iPointsx, iPointsy, iPointsz};

elseif nargin == 4

    [Nx, Ny, Lx, Ly] = varargin{:};

    iPointsx = linspace(-Lx/2, Lx/2, Nx);
    iPointsy = linspace(-Ly/2, Ly/2, Ny);
    samples = {iPointsx, iPointsy};

else

    error('Input variable number should be 4 (2D image) or 6 (3D image)');

end
