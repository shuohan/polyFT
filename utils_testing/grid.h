#ifdef MATLAB_MEX_FILE 
#include "mex.h"
#endif

void accumulateMatrixValue2D(double *matrix, long rowNum, long x, long y, 
        double value);
void accumulateMatrixValue3D(double *matrix, long yNum, long xNum, long x, 
        long y, long z, double value);
double interpolate(double *array, double idx);
long convert(double inputValue, long maxInt);
double convertWithoutShift(double inputValue, long maxInt);
double inverseConvert(long inputValue, long maxInt);

void grid2D(double *kxLoc, double *kyLoc, double *inputReal, 
        double *inputImag, long inputNum, double *dcf, 
        double *outputReal, double *outputImag, long outputNum, 
        double convWidth, double *kernel, int kernelLen);

void grid3D(double *kxLoc, double *kyLoc, double *kzLoc, double *inputReal, 
        double *inputImag, long inputNum, double *dcf, 
        double *outputReal, double *outputImag, long outputNum, 
        double convWidth, double *kernel, int kernelLen);
