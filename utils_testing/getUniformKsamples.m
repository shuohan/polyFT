function ksamples = getUniformKsamples(varargin)
%
% DESCRIPTION
% Generates k space points vectors uniformly.                                     
%
% EXAMPLES
% 3D k-space: ksamples = getUniformKsamples(Nx, Ny, Nz, Lx, Ly, Lz);
% 2D k-space: ksamples = getUniformKsamples(Nx, Ny, Lx, Ly);
%
% INPUT VARIABLES
% - Nx: points number along kx axis                                     
% - Ny: points number along ky axis                                     
% - Nz: points number along kz axis                                     
% - Lx: FOV x axis length                                               
% - Ly: FOV y axis length                                               
% - Lz: FOV z axis length                                               
%
% OUTPUT VARIABLE
% - ksamples: k space vectors                                                  
%
% -------------------------------------------------------------------------
% Modified from Tri Ngo's code @JHU-SOM
% -------------------------------------------------------------------------

if nargin == 6 % 3d k-space
    
    [Nx, Ny, Nz, Lx, Ly, Lz] = varargin{:};

    kxcoors = getCoordinates(Nx, Lx);
    kycoors = getCoordinates(Ny, Ly);
    kzcoors = getCoordinates(Nz, Lz);

    [ksamples(:, :, :, 2), ksamples(:, :, :, 1), ksamples(:, :, :, 3)] = ...
        ndgrid(kxcoors, kycoors, kzcoors);

elseif nargin == 4 % 2d k-space
    
    [Nx, Ny, Lx, Ly] = varargin{:};

    kxcoors = getCoordinates(Nx, Lx);
    kycoors = getCoordinates(Ny, Ly);

    [ksamples(:, :, 2), ksamples(:, :, 1)] = ...
        ndgrid(kxcoors, kycoors);

    ksamples(:, :, 3) = zeros(size(ksamples(:, :, 2)));

else

    error(['For 3D k-space, input: Nx, Ny, Nz, Lx, Ly, Lz\n', ...
        'For 2D k-space, input: Nx, Ny, Lx, Ly.']);

end

% -------------------------------------------------------------------------

function coors = getCoordinates(N, L)
% generate coordinates along one direction
coors = ((0 : N - 1) - floor(N / 2)) / L;
