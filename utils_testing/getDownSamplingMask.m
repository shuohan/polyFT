function [mask, acs] = getDownSamplingMask(peNum, acsLineNum, R)

% DESCRIPTION
% generates GRAPPA sampling pattern.
%
% INPUT VARIABLE
% - peNum: the number of phase encoding lines
%
% INPUT OPTIONS
% - acsLineNum: the number of acs phase encoding lines. 
%   (default: 1/4 * peNum)
% - R: dowsampling rate (default: 2)
%
% OUTPUT VARIABLE
% - mask: logical array, indicates phase encoding line
%   indexes of the sampled data
% - acs: phase encoding line indexes of calibration data

if nargin <= 1
    acsLineNum = round(peNum * 0.25);    
end
if nargin <= 2
    R = 2;
end

R = round(R);

if mod(peNum, 2) == 0 % even
    acsLineNum = acsLineNum + mod(acsLineNum, 2);
else % odd
    acsLineNum = acsLineNum + (mod(acsLineNum, 2) == 0);
end

mask = false(1, peNum);
mask(1 : R : peNum) = true;

startIdx = (peNum - acsLineNum) / 2 + 1;
endIdx = startIdx + acsLineNum - 1;
acs = false(1, peNum);
acs(startIdx : endIdx) = 1;
