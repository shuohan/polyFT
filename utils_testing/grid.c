#include <stdio.h>
#include <math.h>
#include "grid.h"

void grid2D(double *kxLoc, double *kyLoc, double *inputReal, 
        double *inputImag, long inputNum, double *dcf, 
        double *outputReal, double *outputImag, long outputNum, 
        double convWidth, double *kernel, int kernelLen)
{
    long i, j, k;

    double kConvWidth;    // kernel width, in k-space units 
    double dkx, dky, dk;  // distance to the kernel center
    long xConvMin, xConvMax, yConvMin, yConvMax; // the range that
                          // convolution affects for each kspace sample
    double kernelIdx;     // the index of the kernel look-up table
    double kernelValue, real, imag; // kernel value at kernelIdx

    kConvWidth = convWidth / (double)outputNum; 

    for (j = 0; j < outputNum; j++)
    {
        for (k = 0; k < outputNum; k++)
        {
            outputReal[j * outputNum + k] = 0;
            outputImag[j * outputNum + k] = 0;
        }
    }
    
    for (i = 0; i < inputNum; i++)
    {
        xConvMin = convert(kxLoc[i] - kConvWidth, outputNum);
        xConvMax = convert(kxLoc[i] + kConvWidth, outputNum);
        yConvMin = convert(kyLoc[i] - kConvWidth, outputNum);
        yConvMax = convert(kyLoc[i] + kConvWidth, outputNum);
        
        for (j = xConvMin; j <= xConvMax; j++)
        {
            dkx = inverseConvert(j, outputNum) - kxLoc[i];
            for (k = yConvMin; k <= yConvMax; k++)
            {
                dky = inverseConvert(k, outputNum) - kyLoc[i];
                dk = sqrt(dkx * dkx + dky * dky); 

                if (dk <= kConvWidth)   
                {
                    kernelIdx = convertWithoutShift(dk / kConvWidth, kernelLen);
                    kernelValue = interpolate(kernel, kernelIdx);

                    real = kernelValue * inputReal[i] * dcf[i];
                    imag = kernelValue * inputImag[i] * dcf[i];

                    accumulateMatrixValue2D(outputReal, outputNum, j, k, real);
                    accumulateMatrixValue2D(outputImag, outputNum, j, k, imag);
                }
            }
        }
    }
}

void grid3D(double *kxLoc, double *kyLoc, double *kzLoc, double *inputReal, 
        double *inputImag, long inputNum, double *dcf, 
        double *outputReal, double *outputImag, long outputNum, 
        double convWidth, double *kernel, int kernelLen)
{
    long i, j, k, m, idxTmp;

    double kConvWidth;    // width of kernel in k-space units.
    double dkx, dky, dkz, dk;  
    long xConvMin, xConvMax, yConvMin, yConvMax, zConvMin, zConvMax; 
    double kernelIdx;     
    double kernelValue, real, imag;  

    kConvWidth = convWidth / (double)outputNum; 

    for (i = 0; i < outputNum; i++)
    {
        for (j = 0; j < outputNum; j++)
        {
            for (k = 0; k < outputNum; k++)
            {
                idxTmp = i * outputNum * outputNum + j * outputNum + k;
                outputReal[idxTmp] = 0;
                outputImag[idxTmp] = 0;
            }
        }
    }
    
    for (i = 0; i < inputNum; i++)
    {
        xConvMin = convert(kxLoc[i] - kConvWidth, outputNum);
        xConvMax = convert(kxLoc[i] + kConvWidth, outputNum);
        yConvMin = convert(kyLoc[i] - kConvWidth, outputNum);
        yConvMax = convert(kyLoc[i] + kConvWidth, outputNum);
        zConvMin = convert(kzLoc[i] - kConvWidth, outputNum);
        zConvMax = convert(kzLoc[i] + kConvWidth, outputNum);
        
        for (j = xConvMin; j <= xConvMax; j++)
        {
            dkx = inverseConvert(j, outputNum) - kxLoc[i];
            for (k = yConvMin; k <= yConvMax; k++)
            {
                dky = inverseConvert(k, outputNum) - kyLoc[i];
                for (m = zConvMin; m <= zConvMax; m++)
                {
                    dkz = inverseConvert(m, outputNum) - kzLoc[i];
                    dk = sqrt(dkx * dkx + dky * dky + dkz * dkz); 

                    if (dk <= kConvWidth)   
                    {
                        kernelIdx = convertWithoutShift(dk / kConvWidth, 
                                kernelLen);
                        kernelValue = interpolate(kernel, kernelIdx);

                        real = kernelValue * inputReal[i] * dcf[i];
                        imag = kernelValue * inputImag[i] * dcf[i];

                        accumulateMatrixValue3D(outputReal, outputNum, 
                                outputNum, j, k, m, real);
                        accumulateMatrixValue3D(outputImag, outputNum, 
                                outputNum, j, k, m, imag);
                    }
                }
            }
        }
    }
}

void accumulateMatrixValue2D(double *matrix, long rowNum, long x, long y, 
        double value)
/*   
 *   |
 *   |
 * y |    matrix
 *   |
 *   v
 *    ------------->
 *          x
 *  matrix stored as [1st column, 2nd column, ...]
 */
{
    matrix[x * rowNum + y] += value;
}

void accumulateMatrixValue3D(double *matrix, long yNum, long xNum, long x, 
        long y, long z, double value)
/*   
 *   |
 *   |
 * y |    matrix      ^
 *   |               / z
 *   v              /
 *    ------------->
 *          x
 *  matrix stored as [1st column, 2nd column, ...]
 */
{
    matrix[z * xNum * yNum + x * yNum + y] += value;
}

double interpolate(double *array, double idx)
// linear interpolation
{
    int prior;
    double frac;
    double result;

    prior = (int)idx;
    frac = idx - (double)prior;

    result = array[prior] * (1 - frac) + array[prior + 1] * frac;
    return result;
}

long convert(double inputValue, long maxInt)
// if maxInt is even, map value [-0.5, 0.5] to integer [0, maxInt]
// if maxInt is odd, map value [-0.5, 0.5] to integer [0, maxInt - 1]
{
    long center;
    long result;

    center = (int)(maxInt / 2); 
    if (maxInt % 2) // odd
        result = (int)((maxInt - 1) * inputValue + center);
    else // even
        result = (int)(maxInt * inputValue + center);

    if (result < 0)
        result = 0;
    else if (result >= maxInt)
        result = maxInt - 1;

    return result;
}

double convertWithoutShift(double inputValue, long maxInt)
// map double value [0, 1] to double value [0, maxInt - 1]
{
    return inputValue * (double)(maxInt - 1);
}

double inverseConvert(long inputValue, long maxInt)
// if maxInt is even, map integer [0, maxInt] to value [-0.5, 0.5]
// if maxInt is odd, map integer [0, maxInt - 1] to value [-0.5, 0.5]
{
    long center;
    double result;
    
    center = (int)(maxInt / 2); 
    if (maxInt % 2) // odd
        result = (double)(inputValue - center) / (double)(maxInt - 1);
    else // even
        result = (double)(inputValue - center) / (double)maxInt;
    result = (double)(inputValue - center) / (double)maxInt;

    if (result < -0.5)
        result = -0.5;
    else if (result > 0.5)
        result = 0.5;

    return result;
}
