polyFTAddPath;

%% set parameters

SHOW_MESH = true;
SHOW_RECON = true;

kSampleNumX = 2 ^ 5;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

ellipoidResolution = 5;

%% construct an epllipse mesh

[faces, vertices] = getEllipsoidMesh([], ellipoidResolution);

if SHOW_MESH
    showMesh(faces, vertices);
end

%% compute FT of this mesh

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
imageSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, kSampleNumZ, ...
    Lx, Lx, Lz);

tic
FT = polyFT(faces, vertices, kSamples, 'Type', 'mex', 'Parallel', false, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamples) / 3);

%% reconstruct

I = cartesianRecon(FT, kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);

if SHOW_RECON
    figure, 
    showRecon(I, imageSamples); 
end
