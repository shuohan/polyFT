% Compare the polyFT solution of a unit cube mesh with its analytical
% solution (sinc^3)

polyFTAddPath;

%% set parameters 

SHOW_MESH = true;
SHOW_RECON = true;

vertexNumsPerEdge = 8; %  faceNum = (VPE-1)^2* (2 triangles) * (6 faces per cube)

kSampleNumX = 2 ^ 5;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

%% get mesh

[faces, vertices] = getCubeMesh(vertexNumsPerEdge);

if SHOW_MESH
    showMesh(faces, vertices);
end

%% polyFT

[Lx, Ly, Lz] = getFOV(vertices);

kSpaceSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, ...
    Lx, Ly, Lz);
imageSpaceSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, ...
    kSampleNumZ, Lx, Lx, Lz);

tic
FT = polyFT(faces, vertices, kSpaceSamples, 'Type', 'mex', 'Parallel', false, ...
    'outerLoopType', 'faces', 'Verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSpaceSamples) / 3);

%% compare to analytical results (sinc^3)

Wx = 1; Wy = 1; Wz = 1; %edge width of cube
kSpace_sinc3 = Wx * Wy * Wz * ...
    sinc(Wx * kSpaceSamples(:, :, :, 1)) .* ...
    sinc(Wy * kSpaceSamples(:, :, :, 2)) .* ...
    sinc(Wz * kSpaceSamples(:, :, :, 3));
im = fftshift(ifftn(ifftshift(kSpace_sinc3)));

% compute k-space errors
diff_k = abs(kSpace_sinc3 - FT);
RMSE_k = sqrt(mean(diff_k(:).^2));
NRMSE_k = RMSE_k / (max(abs(FT(:))) - min(abs(FT(:))));
max_k = max(diff_k(:));

% compute image space errors
im_polyFT = fftshift(ifftn(ifftshift(FT)));
diff_i = abs(im-im_polyFT);
RMSE_i = sqrt(mean(diff_i(:).^2));
NRMSE_i = RMSE_i / (max(abs(im_polyFT(:))) - min(abs(im_polyFT(:))));
max_i = max(diff_i(:));

disp(' ');
disp( 'Comparison of polyFT and analytical solution:');
disp(' ');
disp([ '  Max k-space error:' , num2str(max_k)]);
disp([ '  k-space RMSE: ' num2str(RMSE_k)]);
disp([ '  k-space NRMSE: ', num2str(NRMSE_k) ]);
disp(' ');
disp([ '  Max image space error:' , num2str(max_i)]);
disp([ '  image space RMSE: ' num2str(RMSE_i) ]);
disp([ '  image space NRMSE: ', num2str(NRMSE_i)] );

%% 3D image reconstruction and display

I = cartesianRecon(FT, kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
IAnalytical = cartesianRecon(kSpace_sinc3, kSampleNumX, kSampleNumY, kSampleNumZ, ...
    Lx, Ly, Lz);
    
if SHOW_RECON
    figure('Name', 'polyFT solution');
    showRecon(I, imageSpaceSamples);
    figure('Name', 'analytical solution');
    showRecon(IAnalytical, imageSpaceSamples); 
end
