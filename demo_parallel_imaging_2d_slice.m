polyFTAddPath;

%% set parameters

SHOW_MESH = true;
SHOW_RECON = true;

kSampleNumX = 2 ^ 5;
kSampleNumY = kSampleNumX;

% plane parameters
thickness = 2;
point = [0 0 0];
normal = [0 0 1];

%% get mesh 

load brain_mesh
% [faces, vertices] = getEllipsoidMesh([], 10);

rot = vrrotvec2mat(vrrotvec(normal, [0 0 1]));
faces = [faces, faces(:, 1)];

[faces, vertices] = getSlice(faces, vertices, normal, point, thickness);
% does not work for some thickness. Cork can be used to get a slice.

vertices = (rot * vertices.').';

if SHOW_MESH
    figure, showMesh(faces, vertices, false)
end

% iso2mesh function
[vertices, elements, tmp] = surf2mesh(vertices, faces, ...
    min(vertices), max(vertices), 1, 25);

elements = elements(:, 1 : 4);

%% FT preparation

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, Lx, Ly);
imageSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, Lx, Ly);
imDomainBox = [min(vertices); max(vertices)];
imDomainBox = imDomainBox(:, 1 : 2);

maps = getCoils([kSampleNumX, kSampleNumY]);

intensities = cell(1, size(maps, 3));
gradients = cell(1, size(maps, 3));
faces = cell(1, size(maps, 3));
FT = complex(zeros(size(maps)));
Ireference = zeros(size(maps));
Igrappa = zeros(size(maps));

%% FT for each coil 

for i = 1 : size(maps, 3)
   
    fprintf('Coil %d\n', i); 
    
    intensities{i} = assignIntensityWithMap(elements, vertices, ...
        imDomainBox, maps(:, :, i));
    [faces{i}, gradients{i}] = simplifyElements(elements, vertices, ...
        intensities{i});

    if SHOW_MESH
        figure('Name', sprintf('coil %d', i)),
        showMesh(faces{i}, vertices, false, gradients{i});
        pause(0.1)
    end
    
    tic
    FT(:, :, i) = polyFT(faces{i}, vertices, kSamples, 'Type', 'mex', ...
        'Parallel', true, 'OuterLoopType', 'faces', 'FacesType', ...
        'matrix', 'verbose', 1, 'gradients', gradients{i});
    t = toc;

    printTimingInfo(t, size(faces{i}, 1), numel(kSamples) / 3);

end

%% grappa

[mask, acs] = getDownSamplingMask(kSampleNumX);
acsRep = repmat(acs, [kSampleNumX, 1, size(maps, 3)]);
calib = reshape(FT(acsRep), [kSampleNumX, sum(acs), size(maps, 3)]);

down = FT;
down(~repmat(mask, [kSampleNumX, 1, size(maps, 3)])) = 0;

% filled = grappa(down, calib, ~mask, 2, 9);
filled = GRAPPA(down, calib, [5, 5], 0.01);

%% reconstruct

for i = 1 : size(maps, 3)
    Ireference(:, :, i) = cartesianRecon(FT(:, :, i), kSampleNumX, ...
        kSampleNumY, Lx, Ly, thickness);
    Igrappa(:, :, i) = cartesianRecon(filled(:, :, i), kSampleNumX, ...
        kSampleNumY, Lx, Ly, thickness);
end

if SHOW_RECON
    for i = 1 : size(maps, 3)
        figure('Name', sprintf('coil %d', i)), imshow(Ireference(:, :, i))
    end
    figure('name', 'GRAPPA reconstruction'), imshow(sos(Igrappa))
    figure('name', 'reference'), imshow(sos(Ireference))
    figure('name', 'x40 difference'), ...
        imshow(abs(sos(Ireference) - sos(Igrappa)) * 40)
end
