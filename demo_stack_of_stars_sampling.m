polyFTAddPath;

%% set parameters 

SHOW_MESH = true;
SHOW_RECON = true;

% radial k space sampling parameters
sampleNum = 128; % odd number suggested
projectionNum = ceil(sampleNum * pi / 2);
stackHeight = sampleNum;

% uniform k space sampling parameters
Nx = sampleNum;
Ny = Nx;
Nz = Nx;

ellipsoidResolution = 5;

%% construct an epllipse mesh

[faces, vertices] = getEllipsoidMesh([], ellipsoidResolution);

if SHOW_MESH
    showMesh(faces, vertices);
end

%% compute radial sampling FT of this mesh

[Lx, Ly, Lz] = getFOV(vertices);

fprintf('\nRadial sampling...\n');

kSamplesRadial = getStackOfRadialKpoints(sampleNum, projectionNum, ...
    stackHeight, Lx, Lz);
imSamplesRadial = getImageSpaceSamples(sampleNum, sampleNum, stackHeight, ...
    Lx, Lx, Lz);

tic
FTRadial = polyFT(faces, vertices, kSamplesRadial, 'type', 'mex', ...
    'parallel', true, 'Verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamplesRadial) / 3);

%% compute uniform sampling FT of this mesh

fprintf('\nUnifrom sampling...\n');

kSamplesUniform = getUniformKsamples(Nx, Ny, Nz, Lx, Ly, Lz);
imSamplesUniform = getImageSpaceSamples(Nx, Ny, Nz, Lx, Lx, Lz);

tic
FTUniform = polyFT(faces, vertices, kSamplesUniform, 'type', 'mex', ...
    'parallel', true, 'verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamplesUniform) / 3);

%% reconstruct radial sampling

dataNum = sampleNum * projectionNum * stackHeight;
kSamplesRadial = reshape(kSamplesRadial, [dataNum, 3]);
FTRadial = reshape(FTRadial, [dataNum, 1]);

dcf = voronoiDcf(kSamplesRadial(1 : sampleNum * projectionNum, 1 : 2));
dcf = repmat(dcf, [stackHeight, 1]);
IRadial = griddingRecon(kSamplesRadial, FTRadial, sampleNum, Lx, Ly, Lz, dcf);
IRadial = IRadial / max(IRadial(:));

if SHOW_RECON
    figure('Name', 'Radial Sampling Test');
    showRecon(IRadial, imSamplesRadial, 0.6);
end

%% reconstruct uniform sampling

IUniform = cartesianRecon(FTUniform, Nx, Ny, Nz, Lx, Ly, Lz);
IUniform = IUniform / max(IUniform(:));

if SHOW_RECON
    figure('Name', 'Uniform Sampling Test');
    showRecon(abs(IUniform), imSamplesUniform, 0.6); 
end

%% show difference 

idx = round((size(IUniform, 3) + 1) / 2);
sliceUniform = imrotate(IUniform(:, :, idx), -90);
sliceRadial = imrotate(IRadial(:, :, idx), -90);

if SHOW_RECON
    figure('Name', 'central slice of uniform sampling data'), 
        imshow(sliceUniform);
    figure('Name', 'central slice of radial sampling data'), 
        imshow(sliceRadial);
    figure('Name', 'x20 difference of central slices'), 
        imshow(abs(sliceUniform - sliceRadial) * 20);
end