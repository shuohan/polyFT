function [faces, vertices] = getDodecahedronMesh(meshType, facesType)

% -------------------------------------------------------------------------
%
% DESCRIPTION
%    Get meshes of a dodecahedron
%
% INPUT VARS
%    ~ meshType: 0 (default),1,2 - Type fo mesh
%       0 - faces with 5 vertices (homogeneous); faces -> matrix or cell
%       1 - faces with 3,4 or 5 vertices (heteregenous); faces -> cell 
%       2 - faces with 3 vertices (homogeneous); faces -> matrix or cell
%
%    ~ facesType: 'matrix' (default) or 'cell'. 
%
% -------------------------------------------------------------------------
% Daniel Herzka @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
% -------------------------------------------------------------------------

%% Determine output type

DISP_MESH = false; %dbg

if nargin<1
    meshType=0;
end;

if nargin<2
    facesType= 'matrix';
end

meshType=meshType(1);
if ~any(meshType==[0,1,2])
    meshType=0;
end
if ~any(strcmpi(facesType, {'matrix','cell'}))
    facesType = 'matrix';
end

%% Make dodecahedron with a homogeneous pentagonal faces matrix (returns matrix or cell)
% not sure if directionality matters (e.g. nor sure all faces are defined clockwise or counterclockwise)

phi = (1 + sqrt(5)) / 2;

vertices = [ ... 
    +1, +1, +1 ;... % 1 red
    -1, +1, +1 ;...
    +1, -1, +1 ;...
    +1, +1, -1 ;...
    -1, -1, +1 ;... % 5
    -1, +1, -1 ;...
    +1, -1, -1 ;...
    -1, -1, -1 ;... 
    0, +1/phi, +phi;... % 9 green
    0, -1/phi, +phi;...
    0, +1/phi, -phi;...
    0, -1/phi, -phi;... 
    +1/phi, +phi, 0;... % 13 blue
    -1/phi, +phi, 0;...
    +1/phi, -phi, 0;...
    -1/phi, -phi, 0;... 
    +phi, 0, +1/phi;... % 17 magenta
    -phi, 0, +1/phi;...
    +phi, 0, -1/phi;...
    -phi, 0, -1/phi;... % 20
 ]; 
   
faces = [...
    1 17  3 10  9 ; ...
    3 17 19  7 15; ...
    3 15 16  5 10; ...
    2  9 10  5 18;...
    5 16  8 20 18;...
    8 16 15  7 12 ;...
    1  9  2 14 13 ; ...
    6 11  4 13 14; ...
    2 18 20  6 14;...
    6 20  8 12 11 ;...
    1 13  4 19 17;...
    4 11 12  7 19 ];

faces = faces(:, end : -1 : 1);

if meshType==0 

    if DISP_MESH
        figure;
        plot3(vertices(1:8,1), vertices(1:8,2), vertices(1:8,3), 'ro', 'markersize', 8, 'markerfacecolor', 'r');
        hold on
        plot3(vertices(9:12,1), vertices(9:12,2), vertices(9:12,3), 'go', 'markersize', 8, 'markerfacecolor', 'g');
        plot3(vertices(13:16,1), vertices(13:16,2), vertices(13:16,3), 'bo', 'markersize', 8, 'markerfacecolor', 'b');
        plot3(vertices(17:20,1), vertices(17:20,2), vertices(17:20,3), 'mo', 'markersize', 8, 'markerfacecolor', 'm');
        
        cmap = copper(size(faces,1));
        
        for i=1:size(faces,1)
            patch(vertices(faces(i,:),1), vertices(faces(i,:),2), vertices(faces(i,:),3), cmap(i,:), 'edgecolor', 'w');
        end
        
        xlim([-phi,phi]);
        ylim([-phi,phi]);
        zlim([-phi,phi]);
        
        grid on
        axis equal
        
        % Other option for display:
        %figure;
        %trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 1:size(faces,1), 'edgecolor', 'w');
        %colormap(cmap);
        %axis equal
    end

    
    if strcmpi(facesType, 'cell')
        faces = mat2cell(faces, ones(size(faces,1),1),size(faces,2));
    end
    
    return;
end


%% Define a dodecahedron with a heterogeneous faces matrix (returns a cell)

if meshType==1
    
    facesHet = [...
        1 17  3 10  0;...
        1 10  9  0  0;...
        3 17 19  7 15;...
        3 15 16  5  0;...
        3 5 10   0  0;...
        2 9 10   5 18;...
        5 16  8 20  0;...
        5 20 18  0  0;...
        8 16 15  7 12;...
        1  9  2 14  0;...
        1 14 13  0  0;...
        6 11  4 13 14;...
        2 18 20  6  0;...
        2  6 14  0  0;...
        6 20  8 12 11;...
        1 13  4 19  0;...
        1 19 17  0  0;...
        4 11 12  7 19];
    
    facesHet = facesHet(:, end : -1 : 1);
 
    facesHet = mat2cell(facesHet, ones(size(facesHet,1),1),size(facesHet,2));
    faces = cellfun(@(x) x(x~=0), facesHet, 'uniformoutput', 0); % remove zeros
    
    if DISP_MESH
        figure;
        plot3(vertices(1:8,1), vertices(1:8,2), vertices(1:8,3), 'ro', 'markersize', 8, 'markerfacecolor', 'r');
        hold on
        plot3(vertices(9:12,1), vertices(9:12,2), vertices(9:12,3), 'go', 'markersize', 8, 'markerfacecolor', 'g');
        plot3(vertices(13:16,1), vertices(13:16,2), vertices(13:16,3), 'bo', 'markersize', 8, 'markerfacecolor', 'b');
        plot3(vertices(17:20,1), vertices(17:20,2), vertices(17:20,3), 'mo', 'markersize', 8, 'markerfacecolor', 'm');
        
        xlim([-phi,phi]);
        ylim([-phi,phi]);
        zlim([-phi,phi]);
        
        grid on
        axis equal
        cmap = copper(size(faces,1));
        
        for i=1:size(faces,1)
            patch(vertices(faces{i},1), vertices(faces{i},2), vertices(faces{i},3), cmap(i,:), 'edgecolor', 'w');
        end
    end
    
    return;
end

%% Define a dodecahedron with a homogeneous triangular faces matrix (matrix or cell)
% [ v1 v2 v3; v1 v3 v4; v1 v4 v5]

if meshType==2
    facesTri =[faces(:,1:3), faces(:,[1,3,4]), faces(:,[1,4,5])];
    facesTri = reshape(facesTri,[12,3,3]);
    facesTri = permute(facesTri, [3 1 2]);
    faces    = reshape(facesTri, [36,3]);
    
    if strcmpi(facesType, 'cell') 
        faces = mat2cell(faces, ones(size(faces,1),1),size(faces,2));
        faces = cellfun(@(x) x(x~=0), faces, 'uniformoutput', 0); % remove zeros
    end
    
    if DISP_MESH
        figure;
        plot3(vertices(1:8,1), vertices(1:8,2), vertices(1:8,3), 'ro', 'markersize', 8, 'markerfacecolor', 'r');
        hold on
        plot3(vertices(9:12,1), vertices(9:12,2), vertices(9:12,3), 'go', 'markersize', 8, 'markerfacecolor', 'g');
        plot3(vertices(13:16,1), vertices(13:16,2), vertices(13:16,3), 'bo', 'markersize', 8, 'markerfacecolor', 'b');
        plot3(vertices(17:20,1), vertices(17:20,2), vertices(17:20,3), 'mo', 'markersize', 8, 'markerfacecolor', 'm');
        
        xlim([-phi,phi]);
        ylim([-phi,phi]);
        zlim([-phi,phi]);
        
        grid on
        axis equal
        cmap = copper(size(faces,1));
        
        if ~iscell(faces)
            for i=1:size(faces,1)
                patch(vertices(faces(i,:),1), vertices(faces(i,:),2), vertices(faces(i,:),3), cmap(i,:), 'edgecolor', 'w');
            end
            
            % Other option for display:
            %figure;
            %trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 1:size(faces,1), 'edgecolor', 'w' );
            %colormap(cmap);
            %axis equal
        else
            for i=1:size(faces,1)
                patch(vertices(faces{i},1), vertices(faces{i},2), vertices(faces{i},3), cmap(i,:), 'edgecolor', 'w');
            end
        end

    end
    
    return;
end

