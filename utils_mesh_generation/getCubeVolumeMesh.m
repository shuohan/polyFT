function [elements, vertices] = getCubeVolumeMesh(vNumPerSurfEdge)

% - vNumPerSurfEdge: vertex number per surface edge; 

startPoint = -0.5;
endPoint = 0.5;

x = linspace(startPoint, endPoint, vNumPerSurfEdge);
y = linspace(startPoint, endPoint, vNumPerSurfEdge);
z = linspace(startPoint, endPoint, vNumPerSurfEdge);

[xx, yy, zz] = meshgrid(x, y, z);
vertices = [xx(:), yy(:), zz(:)];
elements = delaunay(xx(:), yy(:), zz(:));
