function [faces, vertices] = getCubeMesh(xsize, ysize, zsize)
% -------------------------------------------------------------------------
%
% DESCRIPTION
%    UTIL Get meshes of a cube                                                  
%
% INPUT VARS
%    ~ xsize: the points number along x axis                               
%    ~ ysize: the points number along y axis                               
%    ~ zsize: the points number along z axis                               
%
% OUTPUT VARS
%    ~ faces: the faces of this cube. 3 by m matrix. Each column contains t
%    he vertices indexes from the variable vertices, which forms this face.
%     The faces are all triangular.                                        
%    ~ vertices: the vertices of the cube. 3 by n matrix. Each column is th
%    e poison vector of this vertex.                                       
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM 
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
%  Modified from Tri Ngo's code. 
% -------------------------------------------------------------------------

if nargin == 1
    ysize = xsize;
    zsize = xsize;
end

startp = -0.5;
endp = 0.5;
x = linspace(startp, endp, xsize);
y = linspace(startp, endp, ysize);
z = linspace(startp, endp, zsize);

if numel(x)<=1, x = [startp, endp]; end
if numel(y)<=1, y = [startp, endp]; end
if numel(z)<=1, z = [startp, endp]; end

[X, Y, Z] = meshgrid(x, y, z);

vertices = [X(:), Y(:), Z(:)];
faces = (convhull(vertices)); 
