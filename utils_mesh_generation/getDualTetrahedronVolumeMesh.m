function [elements, vertices, intensities] = getDualTetrahedronVolumeMesh

vertices = [-0.5, -0.5, -0.5; 
            0.5, -0.5, -0.5;
            -0.5, 0.5, -0.5;
            -0.5, -0.5,  0.5;
            0.5, 0.5, 0.5];
        
elements = [1, 2, 3, 4; 
            2, 3, 4, 5];
         
intensities = [0.5, 1];       
