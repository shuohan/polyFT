function [faces, vertices] = getEllipsoidMesh(eparm, res)

% -------------------------------------------------------------------------
%
% DESCRIPTION
%    Get meshes of a cube                                                  
%
% INPUT VARS
%    ~ eplpara: the parameters specifying this ellipsoid. If it is [], use 
%    the default parameters.
%    ~ res: res + 1 is the points number along x, y, z axis.                                
%
% OUTPUT VARS
%    ~ faces: the faces of this cube. 3 by m matrix. Each column contains t
%    he vertices indexes from the variable vertices, which forms this face.
%     The faces are all triangular.                                        
%    ~ vertices: the vertices of the cube. 3 by n matrix. Each column is th
%    e poison vector of this vertex.                                       
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
%  Modified from Tri Ngo's code.
% -------------------------------------------------------------------------

if isempty(eparm)
       
    eparm =[ 0       % delta_x
             0       % delta_y
             0       % delta_z
             0.50    % a
             0.75    % b
             0.30    % c
             0       % phi
             0       % theta
             0       % psi
             1 ];    % rho
end

[x, y, z] = ellipsoid( eparm(1), eparm(2), eparm(3),...
    eparm(4), eparm(5), eparm(6), res);

faces = (convhull(x,y,z));
vertices = [x(:), y(:), z(:)];   % vertices, 3 by n
