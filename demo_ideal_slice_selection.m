polyFTAddPath;

%% set parameters 

SHOW_MESH = true;
SHOW_RECON = true;

kSampleNumX = 2 ^ 6;
kSampleNumY = kSampleNumX;

% plane parameters
thickness = 10;
point = [0 0 0];
normal = [0 0 1];

%% get mesh slice

load brain_mesh

rot = vrrotvec2mat(vrrotvec(normal, [0 0 1]));
faces = [faces, faces(:, 1)];

[faces, vertices] = getSlice(faces, vertices, normal, point, thickness);
vertices = (rot * vertices.').';

if SHOW_MESH
    showMesh(faces, vertices, false);
end

%% compute FT of this mesh

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, Lx, Ly);

tic
FT = polyFT(faces, vertices, kSamples, 'Type', 'mex', 'Parallel', true, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamples) / 3);

%% reconstruct

I = cartesianRecon(FT, kSampleNumX, kSampleNumY, Lx, Ly, thickness);

if SHOW_RECON
    figure, imshow(abs(I(end : -1 : 1, :)))
end
