% Calculate the polyhedral FT of publically available brain meshes.
%
% The mesh is generated from Open Access Series of Imaging Studies (OASIS 
% http://www.oasis-brains.org) using TOADS and CRUISE algorithems
% (https://www.iacl.ece.jhu.edu/CRUISE)

clear
close all
clc

polyFTAddPath;

%% set parameters

SHOW_MESH = true;
SHOW_RECON = true;

kSampleNumX = 2 ^ 3;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

%% load mesh

load brain_mesh

if SHOW_MESH
    showMesh(faces, vertices, false);
end

%% compute FT

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
imageSpaceSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, kSampleNumZ,...
    Lx, Ly, Lz);

tic
FT = polyFT(faces, vertices, kSamples, 'Type', 'mex', 'Parallel', true, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'verbose', 1);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamples) / 3);

%% reconstruct 3D image

I = cartesianRecon(FT, kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);
    
if SHOW_RECON
    figure('Name', 'Brain Mesh (poly FT)');
    showRecon(I, imageSpaceSamples);
end
