# PolyFT

PolyFT performs 3D Fourier transform of a polyhedron, as specified by Ngo TM et al. ("Realistic Analytical Polyhedral MRI Phantoms". Magnetic Resonance in Medicine 2015), and based on the expressions provided by Kormska J. ("Algebraic expressions of shape amplitudes of polygons and polyhedra". Optik 1988;80:171-183).  

It is mainly developed for MRI acquisition and reconstruction simulation. Rather than simple shapes, such as Shepp-Logan phantoms (Koay et al. "Three‐dimensional analytical magnetic resonance imaging phantom in the Fourier domain." Magnetic Resonance in Medicine 2007;58:430-436), polyFT can represent physiologically relevant shapes. It currently supports uniform intensity simulation as well as non-uniform intensity simulation. 

The package now contains demonstrations regarding:

* 3D acquisition
* 2D slice acquisition
* multi-coil acquisition and parallel imaging reconstruction
* non-Cartesian sampling

## Installation 

1. PolyFT runs under MATLAB. It contains pure-MATLAB implementation and mex implementation. Both implementations have comparable running time. 

To compile mex files, run the script:

    compile_mex.m

2. To run *demo\_parallel\_imaging\_2d\_slice.m* and *demo\_parallel\_imaging\_3d.m*, [ESPIRiT](http://www.eecs.berkeley.edu/~mlustig/Software.html) by Lustig needs to be installed.

3. To simulate non-uniform intensities, a volume mesh should be calculated based on the given surface mesh. [iso2mesh](http://iso2mesh.sourceforge.net) is recommended.

4. To run *demo\_parallel\_imaging\_2d\_slice.m*, *demo\_ideal\_slice\_selection.m*, and *demo\_non\_ideal\_slice\_selection.m*, a slice of the mesh should be extracted. The package includes a function *getSlice.m*, but sometimes the result is not compatible with iso2mesh. (There might be an unspotted bug in getSlice.m). Alternatively, [cork](https://github.com/gilbo/cork.git) can be used to extract a slice. There is a demo under directory *cork\_slice* showing how to use cork.

5. The package also includes a gridding reconstruction implementation. It is modified from [Gridding Functions](http://mrsrl.stanford.edu/~brian/gridding/) by Hargreaves et al..

## Examples

Check the demos to see how to use the package. 
