polyFTAddPath;

%% set parameters

SHOW_MESH = false;
SHOW_RECON = true;

kSampleNumX = 2 ^ 5;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

%% load a volume mesh

load volume_mesh

if SHOW_MESH
    figure, tetramesh(elements, vertices) 
end

%% get coils

maps = getCoils([kSampleNumX, kSampleNumY]);
volumeMaps = cell(1, size(maps, 3));

for i = 1 : length(volumeMaps)
    volumeMaps{i} = repmat(maps(:, :, i), [1, 1, kSampleNumZ]);
end

%% kspace sample coornidates

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, ...
    Lx, Ly, Lz);

imageSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, kSampleNumZ, ...
    Lx, Lx, Lz);
imDomainBox = [cellfun(@(x) min(x), imageSamples); ...
    cellfun(@(x) max(x), imageSamples)];

[mask, acs] = getDownSamplingMask(kSampleNumX);

%% polyFT

FT = cell(1, length(volumeMaps));
FT = cellfun(@(x) zeros([kSampleNumX, kSampleNumY, kSampleNumZ]), FT, ...
    'Uniformoutput', false);

for i = 1 : length(volumeMaps)
    
    fprintf('Coil %d\n', i);
    
    intensities = assignIntensityWithMap(elements, vertices, imDomainBox, ...
        volumeMaps{i});
    [facesTmp, gradients] = simplifyElements(elements, vertices, intensities);
    
    tic
    FT{i} = polyFT(facesTmp, vertices, kSamples, 'Type', 'optimized', 'Parallel', false, ...
        'OuterLoopType', 'faces', 'FacesType', 'matrix', 'Gradients', gradients, ...
        'Verbose', 2);
    t = toc;
    
    printTimingInfo(t, size(facesTmp, 1), numel(kSamples) / 3);

end

%% get centeral slice

FT2 = cell(1, length(volumeMaps));
slice = complex(zeros([kSampleNumX, kSampleNumY, length(volumeMaps)]));

for i = 1 : length(volumeMaps)
    factor = kSampleNumX * kSampleNumY * kSampleNumZ / (Lx *  Ly * Lz); 
    FT2{i} = FT{i} * factor;
    FT2{i} = fftshift(ifft(ifftshift(FT2{i}, 3), [], 3), 3);
    slice(:, :, i) = FT2{i}(:, :, round(kSampleNumZ / 2));
end

%% reconstruction 

acsRep = repmat(acs, [kSampleNumX, 1, length(volumeMaps)]);
calib = reshape(slice(acsRep), [kSampleNumX, sum(acs), length(volumeMaps)]);
sliceDownsampled = slice;
sliceDownsampled(~repmat(mask, [kSampleNumX, 1, size(maps, 3)])) = 0;

% sliceFilled = grappa(sliceDownsampled, calib, ~mask, 2, 9);
sliceFilled = GRAPPA(sliceDownsampled, calib, [5, 5], 0.01);

I = zeros(size(sliceFilled));

for i = 1 : length(volumeMaps)
    I(:, :, i) = fftshift(ifft2(ifftshift(sliceFilled(:, :, i))));
end

if SHOW_RECON
    figure, imshow(sos(I))
end

%%

I2 = zeros(size(slice));

for i = 1 : length(volumeMaps)
    I2(:, :, i) = fftshift(ifft2(ifftshift(sliceDownsampled(:, :, i))));
end

if SHOW_RECON
    figure, imshow(sos(I2)); 
end

%%

I3 = zeros(size(slice));

for i = 1 : length(volumeMaps)
    
    tmp = cartesianRecon(FT{i}, kSampleNumX, kSampleNumY, kSampleNumZ, ...
        Lx, Ly, Lz);
    I3(:, :, i) = tmp(:, :, round(kSampleNumZ / 2));
    
end

figure, imshow(sos(I3));
a = sos(maps);

%%
figure, imshow([sos(I3), sos(I2), sos(I)] / max(a(:)))
