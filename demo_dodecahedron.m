% Generate several dodecahedral meshes and calculate the polyhedral FT for
% each one to compare differences. 

polyFTAddPath;

SHOW_MESH = false;
SHOW_RECON = true;

%% set parameters 

testAct = [1 : 12]; %1 : 12; % [any subgroup 2-12 plus 1]

kSampleNumX = 2 ^ 3;
kSampleNumY = kSampleNumX;
kSampleNumZ = kSampleNumX;

parallelOptions = cell(1, max(testAct));
facesTypeOptions = cell(1, max(testAct));
typeOptions = cell(1, max(testAct));
outerLoopTypeOptions = cell(1, max(testAct));
verbose = 2;

faces = cell(1,  max(testAct));
vertices = cell(1,  max(testAct));

FT = cell(1,  max(testAct));
I = cell(1,  max(testAct));
t = zeros(1, max(testAct));

%% design different tests to test performance of polyhedral phantom 

% -------------------------------------------------------------------------
% Test #1: faces <matrix> with pentagonal facets
%   parallel computation along the k-samples direction

id = 1;

[faces{id}, vertices{id}] = getDodecahedronMesh(0, 'matrix');

parallelOptions{id} = true;
facesTypeOptions{id} = 'matrix';
outerLoopTypeOptions{id} = 'kSamples';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #2: faces <cell> with pentagonal facets
%   parallel computation along the k-samples direction

id = 2;

[faces{id}, vertices{id}] = getDodecahedronMesh(0, 'cell');

parallelOptions{id} = true;
facesTypeOptions{id} = 'cell';
outerLoopTypeOptions{id} = 'kSamples';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #3:  faces <cell> with variable number of vertices per facet (3, 4 or 5) 
%   parallel computation along the k-samples direction

id = 3;

[faces{id}, vertices{id}] = getDodecahedronMesh(1, 'cell');

parallelOptions{id} = true;
facesTypeOptions{id} = 'cell';
outerLoopTypeOptions{id} = 'kSamples';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #4: faces <matrix> with triangular facets
%   parallel computation along the k-samples direction

id = 4;

[faces{id}, vertices{id}] = getDodecahedronMesh(2, 'matrix');

parallelOptions{id} = true;
facesTypeOptions{id} = 'matrix';
outerLoopTypeOptions{id} = 'kSamples';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #5: faces <matrix> with pentagonal facets
%   parallel computation along the faces direction

id = 5;

[faces{id}, vertices{id}] = getDodecahedronMesh(0, 'matrix');

parallelOptions{id} = true;
facesTypeOptions{id} = 'matrix';
outerLoopTypeOptions{id} = 'faces';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
%  Test #6: faces <cell> with pentagonal facets
%   parallel computation along the faces direction

id = 6;

[faces{id}, vertices{id}] = getDodecahedronMesh(0, 'cell');

parallelOptions{id} = true;
facesTypeOptions{id} = 'cell';
outerLoopTypeOptions{id} = 'faces';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #7:  faces <cell> with variable number of vertices per facet (3, 4 or 5) 
%   parallel computation along the faces direction

id = 7;

[faces{id}, vertices{id}] = getDodecahedronMesh(1, 'cell');

parallelOptions{id} = true;
facesTypeOptions{id} = 'cell';
outerLoopTypeOptions{id} = 'faces';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #8:  faces <martix> with triangular facets
%   parallel computation along the faces direction

id = 8;

[faces{id}, vertices{id}] = getDodecahedronMesh(2, 'matrix');

parallelOptions{id} = true;
facesTypeOptions{id} = 'matrix';
outerLoopTypeOptions{id} = 'faces';
typeOptions{id} = 'optimized';

% -------------------------------------------------------------------------
% Test #9:  faces with pentagonal facets
%   No parallel computation (straight for-loop implementation);

id = 9;

[faces{id}, vertices{id}] = getDodecahedronMesh(0, 'matrix');

facesTypeOptions{id} = 'matrix'; % does not affet the behaviour of polyFT
typeOptions{id} = 'normal';
outerLoopTypeOptions{id} = 'faces'; % does not affet the behaviour of polyFT
parallelOptions{id} = false; % does not affet the behaviour of polyFT

% -------------------------------------------------------------------------
% Test #10:  faces with variable number of vertices per facet (3, 4 or 5) 
%   No parallel computation (straight for-loop implementation)

id = 10;

[faces{id}, vertices{id}] = getDodecahedronMesh(1, 'cell');

facesTypeOptions{id} = 'cell'; % does not affet the behaviour of polyFT
typeOptions{id} = 'normal';
outerLoopTypeOptions{id} = 'faces'; % does not affet the behaviour of polyFT
parallelOptions{id} = false; % does not affet the behaviour of polyFT

% -------------------------------------------------------------------------
% Test #11: faces <matrix> with triangluar facets
%   parallel computation along the faces direction using mex function

id = 11;

[faces{id}, vertices{id}] = getDodecahedronMesh(2, 'matrix');

parallelOptions{id} = true;
facesTypeOptions{id} = 'matrix';
typeOptions{id} = 'mex';
outerLoopTypeOptions{id} = 'faces';

% -------------------------------------------------------------------------
% Test #12: faces <matrix> with triangular facets
%   No parallel computation using mex function

id = 12;

[faces{id}, vertices{id}] = getDodecahedronMesh(2, 'matrix');

parallelOptions{id} = false;
facesTypeOptions{id} = 'matrix';
typeOptions{id} = 'mex';
outerLoopTypeOptions{id} = 'faces';

%% show meshes

for i = testAct
    if SHOW_MESH
        showMesh(faces{i}, vertices{i});
    end
end

%% polyFT

[Lx, Ly, Lz] = getFOV(vertices{1});

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz); 
imageSamples = getImageSpaceSamples(kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);

for i =  testAct

    fprintf('Test %d\n', i);

    tic
    FT{i} = polyFT(faces{i}, vertices{i}, kSamples, ...
        'parallel', parallelOptions{i}, ...
        'type', typeOptions{i}, ...
        'outerLoopType', outerLoopTypeOptions{i}, ...
        'facesType', facesTypeOptions{i}, ...
        'verbose', verbose);
    t(i) = toc;
    
    printTimingInfo(t(i), size(faces{i}, 1), numel(kSamples) / 3);
    
    fprintf('\n');    
end

%% reconstruct 3D images

for i = testAct
    
    I{i} = cartesianRecon(FT{i}, kSampleNumX, kSampleNumY, kSampleNumZ, Lx, Ly, Lz);   
 
    if SHOW_RECON
        figure,
        showRecon(I{i}, imageSamples)
    end

end

%% display timing and errors

fprintf('Errors:\n\n');

for i = testAct

    fprintf('Duration (test %d, #faces = %d): %.4f seconds\n',...
        i, size(faces{i},1), t(i));

    if i~=testAct(1)        
        err = FT{testAct(1)} - FT{i};
        err = sqrt((err(:)' * err(:)) / length(err(:)));
        
        fprintf('Error vs. test 1: (test %d, #faces = %d): %e\n', ...
            i, size(faces{i},1), err);
    end

    fprintf('\n');

end
