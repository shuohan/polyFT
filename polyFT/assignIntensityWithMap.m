function intensities = assignIntensityWithMap(elements, vertices, ...
    imDomainBox, map)

% DESCRIPTION
%   applies an intensity map to the tetrahedral mesh. The value of 
%   map(1, 1, 1) will be assigned to the position of the smallest corner of
%   the boundary cuboid specified by imDomainBox. The value of map(end,
%   end, end) will be assigned to the largest corner. Each element will
%   be assinged a map value according to the position of its center.
%   The center position will be rounded to the nearest map pixel whose
%   value will be assigned to this element.
% 
% INPUTS
% - elements: elementNum x 4 matrix. Each row contains 4 vertex indices,
%   representing a tetrahedron.
% - vertices: vertexNum x 3 matrix. Each row is a vertex vector.
% - imDomainBox: 3 x 2 matrix. Assume the mesh is enclosed by a cuboid. 
%   The first row is the coordinate of the smallest corner of the cuboid.
%   The second row is the coordinate of the largest corner of the cuboid.
% - map: pixelized intensity map. map can be either 3D or 2D. If map is a 
%   2D matrix, the z coordinates of centers of emelemts will be ignored.
% 
% OUTPUT
% - intensities: 1 x elementNum array, containing corresponding assigned
%   intensity values of elements.

centers = getElementCenters(elements, vertices);
if ndims(map) == 2
    centers = centers(:, 1 : 2);
end
    
idx = discretizePoints(centers, imDomainBox, size(map));

if ndims(map) == 2
    idx = sub2ind(size(map), idx(:, 1), idx(:, 2));
else
    idx = sub2ind(size(map), idx(:, 1), idx(:, 2), idx(:, 3));
end

intensities = map(idx);

% ------------------------------------------------------------------------

function centers = getElementCenters(elements, vertices)

elementsTmp = elements(:);
verTmp = vertices(elementsTmp, :);

centersTmp = reshape(verTmp, [size(elements), size(verTmp, 2)]);
centersTmp = sum(centersTmp, 2) / size(centersTmp, 2);
centers = permute(centersTmp, [1, 3, 2]);

% ------------------------------------------------------------------------

function idx = discretizePoints(points, imDomainBox, mapSize)

pointNum = size(points, 1);
startCorner = repmat(imDomainBox(1, :), [pointNum, 1]);
endCorner = repmat(imDomainBox(2, :), [pointNum, 1]);

if length(mapSize) == 2
    mapSize = repmat(mapSize - [1 1], [pointNum, 1]);
else
    mapSize = repmat(mapSize - [1 1 1], [pointNum, 1]);
end

tmp = (points - startCorner) ./ (endCorner - startCorner);
idx = round(tmp .* mapSize) + 1;
