function FT = polyFTCompressedCellFaces(faces, vertices, kSamples, varargin)
%
% DESCRIPTION
% polyFT core funtion. Optimize the pure matlab implementation by removing
% the for loop over faces. Faces is a cell array. Direct usage is NOT
% suggested; call via polyFT.m instead.
%
% INPUT VARS
% - vertices: n x 3 matrix. Each row is the vector representing the 
%   coordinates of a vertex (3D).     
% - faces: m x 1 cell array. Each entry represents the indices of the  
%   vertices that make up each of m faces of the polygon.
% - kSamples: k space vectors. p x 3 matrix. Each row is the k point 
%   vector.                                                                     
%
% OUTPUT VARS
% - FT: Fourier space data sampled at the kSamples.                      
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
% Daniel Herzka @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicinefaces
%  Baltimore, MD
% -------------------------------------------------------------------------

parallel = parseInputs(varargin);

FT = complex(zeros(1, size(kSamples, 2)));

[edgeLengths, edges, edgeUnits] = calcEdges(vertices, faces);

faceNormals = calcFaceUnitormals(edgeUnits);
edgeCenters = calcEdgeCenters(vertices, faces);
edgeNormals = calcInPlaneEdgeNormals(edgeUnits, edgeLengths, faceNormals);
kSquaredNorms = getKSampleSquaredNorms(kSamples);

faceNormals = cell2mat(faceNormals);

if parallel

    parfor i = 1 : size(kSamples, 1)

        kSample = kSamples(i, :);
        kSquaredNorm = kSquaredNorms(i, :);

        FT(i) = calcSingleKSampleFT(kSample, kSquaredNorm, edgeNormals, ...
            edges, faceNormals, edgeCenters, vertices, faces);

    end 

else

    for i = 1 : size(kSamples, 1)

        kSample = kSamples(i, :);
        kSquaredNorm = kSquaredNorms(i, :);

        FT(i) = calcSingleKSampleFT(kSample, kSquaredNorm, edgeNormals, ...
            edges, faceNormals, edgeCenters, vertices, faces);

    end 

end

% ------------------------------------------------------------------------

function parallel = parseInputs(args)

parser = inputParser;
parser.CaseSensitive = false;
parser.addParameter('parallel', true, @(x) isnumeric(x) || islogical(x)); 
parser.parse(args{:});
parallel = parser.Results.parallel;

% -------------------------------------------------------------------------

function squaredNorms = getKSampleSquaredNorms(kSamples)

squaredNorms = sum(kSamples .^ 2, 2);

% ------------------------------------------------------------------------

function [edgeLengths, edges, edgeUnits] = calcEdges(vertices, faces)

% vertices is F-by-1 cell, every entry is a E(f)-by-3 matrix

edges = cellfun(@(x) vertices(x(2 : end), :) - vertices(x(1 : end - 1), :), ...
    faces, 'UniformOutput', false);
[edgeLengths, edgeUnits] = normalizeCellVectors(edges);

% -------------------------------------------------------------------------

function normals = calcFaceUnitormals(edgeUnits)

tmp = cellfun(@(x) cross(x(1, :), x(2, :)), ...
    edgeUnits, 'UniformOutput', false);
[~, normals] = normalizeCellVectors(tmp);

% -------------------------------------------------------------------------

function centers = calcEdgeCenters(vertices, faces)

centers = cellfun(@(x) ...
    (vertices(x(2 : end), :) + vertices(x(1 : end - 1), :)) / 2, ...
    faces, 'UniformOutput', false);

% -------------------------------------------------------------------------

function normals = calcInPlaneEdgeNormals(edgeUnits, edgeLengths, faceNormals)

% calculates the normals of edges in the plane of each face. The normals 
% are multiplied by the corresponding edge lengths.

unitNormals = cellfun(@(x, y) cross(x, repmat(y, [size(x, 1), 1]), 2), ...
    edgeUnits, faceNormals, 'UniformOutput', false); 
normals = cellfun(@(x, y) x .* repmat(y, [1, size(x, 2)]), ...
    unitNormals, edgeLengths, 'UniformOutput', false);

% -------------------------------------------------------------------------

function [vecLen, vecUnit] = normalizeCellVectors(vectors)

vecLen = cellfun(@(x) sqrt(sum(x .* x, 2)), vectors, 'UniformOutput', false);
vecUnit = cellfun(@(x, y) x ./ repmat(y, [1, size(x, 2)]), vectors, vecLen, ...
    'UniformOutput', false);

% -------------------------------------------------------------------------

function FT = calcSingleKSampleFT( kSample, kSquaredNorm, edgeNormals, ...
edges, faceNormals, edgeCenters, vertices, faces)

if sum(abs(kSample)) > 1e-10 % not the center of kspace
    
    [orthogonalIdx, otherIdx] = getConditionalFaceIdx(kSample, faceNormals);

    FT = calcOtherFaceFT(kSample, kSquaredNorm, faceNormals, ...
        edgeNormals, edges, edgeCenters, otherIdx);

    FT = FT + calcOrthogonalFaceFT(kSample, kSquaredNorm, faceNormals, ...
        edgeCenters, vertices, faces, orthogonalIdx);

else

    FT = calcZeroKSampleKT(vertices, faces, faceNormals);

end

% -------------------------------------------------------------------------

function FT = calcZeroKSampleKT(vertices, faces, faceNormals)

faceAreas = calcFaceAreas(vertices, faces, faceNormals);
tmp = cell2mat(cellfun(@(x) vertices(x(1), :), faces, 'UniformOutput', false));
tmp = sum(tmp .* faceNormals, 2);
FT = 1 / 3 * sum(tmp .* faceAreas);

% -------------------------------------------------------------------------

function FT = calcOrthogonalFaceFT(kSample, kSquaredNorm, faceNormals, ...
    edgeCenters, vertices, faces, orthogonalIdx)

if sum(orthogonalIdx)

    faceNormals = faceNormals(orthogonalIdx, :);
    edgeCenters = edgeCenters(orthogonalIdx);
    faces = faces(orthogonalIdx);

    FT = 1i / (2 * pi * kSquaredNorm) ... 
        * func3(kSample, faceNormals, edgeCenters) ...
        .* calcFaceAreas(vertices, faces, faceNormals);

else
    FT = 0;
end

% -------------------------------------------------------------------------

function FT = calcOtherFaceFT(kSample, kSquaredNorm, faceNormals, ...
    edgeNormals, edges, edgeCenters, otherIdx)

faceNormals = faceNormals(otherIdx, :);
edgeNormals = edgeNormals(otherIdx);
edges = edges(otherIdx);
edgeCenters =  edgeCenters(otherIdx);

tmp1 = func1(kSample, kSquaredNorm, faceNormals);
tmp2 = func2(kSample, edgeNormals, edges, edgeCenters);

FT = sum(tmp1 .* tmp2) / -(4 * pi * pi * kSquaredNorm);

% -------------------------------------------------------------------------

function result = func1(kSample, kSquaredNorm, faceNormals)

% performs partial calculation

tmp = faceNormals * kSample.';
result = tmp ./ (kSquaredNorm - tmp .^ 2);

% -------------------------------------------------------------------------

function results = func2(kSample, edgeNormals, edges, edgeCenters)

% performs partial calculation

tmp1 = cellfun(@(x) x * kSample.', edgeNormals, 'UniformOutput', false);
tmp2 = cellfun(@(x) sinc(x * kSample.'), edges , 'UniformOutput', false);
tmp3 = cellfun(@(x) exp(-pi * 2i * x * kSample.'), edgeCenters , ...
    'UniformOutput', false);

results = cellfun(@(x, y, z) sum(x .* y .* z), tmp1, tmp2, tmp3, ...
    'UniformOutput', false);
results = cell2mat(results);

% -------------------------------------------------------------------------

function result = func3(kSample, faceNormals, edgeCenters)

tmp1 = faceNormals * kSample.';
tmp2 = cell2mat(cellfun(@(x) x(1, :), edgeCenters, 'UniformOutput', false));
tmp2 = exp(-pi * 2i * tmp2 * kSample.');
result = tmp1 .* tmp2;

% -------------------------------------------------------------------------
   
function areas = calcFaceAreas(vertices, faces, faceNormals)

tmp = cellfun(@(x) ...
    cross(vertices(x(2 : end), :), vertices(x(1 : end - 1), :), 2), ...
    faces, 'UniformOutput', false);
tmp = cell2mat(cellfun(@(x) sum(x, 1), tmp, 'UniformOutput', false));

areas = 0.5 * abs(sum(faceNormals .* tmp, 2));

% -------------------------------------------------------------------------

function [orthogonalIdx, otherIdx] = getConditionalFaceIdx(kSample, faceNormals)

% calcualtes the indexes of faces which a k vector is ortogonal to and not
% orthogonal to.

zero = 1e-10;
otherIdx = sum(abs(faceNormals * skew(kSample)), 2) >  zero;
orthogonalIdx = ~otherIdx;
