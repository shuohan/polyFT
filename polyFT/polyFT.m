function FT = polyFT(faces, vertices, kSamples, varargin)
%
% DESCRIPTION
%
% Fourier transform of a polyhedron, as specified by Ngo TM et al. (2015, 
% Magnetic Resonance in Medicine, "Realistic Analytical 
% Polyhedral MRI Phantoms"), and based on the expressions provided by 
% Kormska J. (1988, Optik: 80, 171-183 "Algebraic expressions of shape 
% amplitudes of polygons and polyhedra").
%
% Function receives meshes in Face-Vertex representation and returns the 
% Fourier Transform sampled at specific continuous Fourier space or k-space  
% coordinates (kSampleCoordinates). 
%
% Determination of the polyhedral FT is computationally intensive, as 
% there are two separate summations (for loops) in the equations. 
% Parallelization (via parfor) is an option and can be applied along 
% either kSampleCoordinates dimension or the Faces dimension. If none is
% specified, the choice is made automatically. One dimension is vectorized 
% (faster), the other uses the parfor loop.
%
%
% INPUT VARIABLES
%
% - faces: faceNum-by-1 cell array or faceNum-by-edgeNum matrix. Each
%   entry in cell or row in the matrix represents a list of vertex
%   indices. 
% - vertices: vertexNum-by-3 matrix. Each row is the coordinate of this
%   vertex.
% - kSamples: kNum-by-3 matrix of k-space samples. Each row is the
%   coordinate vector of this ksapce sample. OR any dimentional matrix 
%   with one dimension equal to 3. For example, if kSamples is a 4D matrix
%   with size [10, 10, 10, 3], then kSamples(:, :, :, 1), 
%   kSamples(:, :, :, 2), kSamples(:, :, :, 3) are kx, ky, kz 
%   coordinaties, respectively; 
%
% INPUT NAME VALUE PAIRS
%
% - 'parallel': true (default) or false; if true, use matlab parallel
%   computing toolbox (parfor) to boost the computation;
% - 'type': 'mex', (default) 'normal', or 'optimized', the type of core
%   function used to calculate polyFT. 'mex' represents the function
%   implemented in mex, 'normal' represents straightforward pure matlab
%   implementation following the equations provided by the paper (Tri, et
%   al, 2015), and 'optimized' also represents pure matlab implementation,
%   but with intense optimization.
% - 'facesType': 'cell' (default when faces is a cell array) or 'matrix'
%   (default when faces is a matrix); choose 'cell' to use the polyFT core
%   functions optimized for cell array, or 'matrix' for optimization for
%   matrix faces.
% - 'outerLoopType': 'faces' (default when the number of faces is greater
%   than the number of kspace samples) or 'kSamples', the variable that the
%   outmost loop is over. When option 'parallel' is true, using 'faces'
%   means distributing the calculation of FT contributions at all kspace
%   samples of each face among all workers, while 'kSamples' means
%   distributing the calculation of the FT at each kspace sample among all
%   workers. 
% - 'gradients': empty or 1d array with the length equal to faceNum; the
%   intensity difference between inside and outside each face (inside -
%   outside). The gradient of a boundary face is the intensity inside this
%   face - 0.
% - 'verbose': 0, 1, or 2; 0: no message printed out; 1: basic infomation
%   about the numbers of faces, verticices, and k-space samples; 2: 2 plus
%   the name of the function that is being used.
%
% OUTPUT VARIABLE
%
% - FT: The Fourier transform or k-space data in the same size as the 
%   input kSample.
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
% Daniel Herzka @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
% -------------------------------------------------------------------------

[type, facesType, parallel, outerLoopType, gradients, verbose] = ...
    parseInputs(faces, vertices, kSamples, varargin);

if verbose > 0 
    fprintf('Number of faces: %d\n', size(faces, 1));
    fprintf('Number of vertices: %d\n', size(vertices, 1));
    fprintf('Number of k space samples: %d ', numel(kSamples) / 3);
    fprintf('\n')
end

if verbose == 2
    if parallel
        paraTmp = 'true';
    else
        paraTmp = 'false';
    end
    fprintf('Type: %s. FacesType: %s. Parallel: %s. OuterLoopType: %s.\n', ...
        type, facesType, paraTmp, outerLoopType);
    fprintf('Length of gradients: %d\n', numel(gradients));
end

%% preparation 

faces = replicateFirstVertices(faces);
[kSamples, permMatrixRev, kSize] = shapeKPoints(kSamples);

%% function selection to perform polyFT

if strcmpi(type, 'mex')
    if verbose == 2
        disp('Into function polyFTMex...');
    end
    FT = polyFTMex(faces, vertices, kSamples, 'outerLoopType', ...
        outerLoopType, 'parallel', parallel, 'gradients', gradients);
elseif strcmpi(type, 'optimized')  
    if strcmpi(facesType, 'matrix')
        faces = faceCell2Matrix(faces);
    else
        faces = faceMatrix2Cell(faces);
    end
    if strcmpi(outerLoopType, 'faces')
        if verbose == 2
            disp('Into function polyFTCompressedKSamples...');
        end
        FT = polyFTCompressedKSamples(faces, vertices, kSamples, ...
            'parallel', parallel, 'gradients',  gradients);
    else
        if strcmpi(facesType, 'matrix')
            if verbose == 2
                disp('Into function polyFTCompressedMatFaces...');
            end
            FT = polyFTCompressedMatFaces(faces, vertices, kSamples, ...
                'parallel', parallel);
        else
            if verbose == 2
                disp('Into function polyFTCompressedCellFaces...');
            end
            FT = polyFTCompressedCellFaces(faces, vertices, kSamples, ...
                'parallel', parallel);
        end
    end
else
    faces = faceMatrix2Cell(faces);
    if verbose == 2
        disp('Into function polyFTNormal...');
    end
    FT = polyFTNormal(faces, vertices, kSamples);
end

%% restoring the shape of FT matrix to match the original size of kSamples 

FT = revShapeFT(FT, kSize, permMatrixRev);

%% ----------------------------SUPPORT FCNS---------------------------------

function [type, facesType, parallel, outerLoopType, gradients, verbose] = parseInputs(...
    faces, vertices, kSamples, args)

parser = inputParser;
parser.CaseSensitive = false;

err = 'Input variable "faces" should be a numerical matrix or a cell array';
parser.addRequired('faces', @(x) assert(ismatrix(x), err));

err = ['Input variable "vertices" should be a numerical matrix with', ... 
    ' size vertexNum x 3; each row is a 3d vector.'];
parser.addRequired('vertices', @(x)  assert(isnumeric(x) && ismatrix(x) && ...
    size(x, 2) == 3, err));

err = ['Input variable "kSamples" should be a numerical matrix with', ... 
    ' at least one dimension equal to 3.'];
parser.addRequired('kSamples', @(x) assert(isnumeric(x) && ...
    any(size(x) == 3), err));

err = ['Optional input "parallel" should be a number of a logical number', ...
    ' (true or false)'];
parser.addParameter('parallel', true, @(x) assert(isnumeric(x) || islogical(x), ...
    err)); 

expectedTypes = {'mex', 'normal', 'optimized'};
parser.addParameter('type', expectedTypes{1}, ...
    @(x) any(validatestring(x, expectedTypes)));

expectedFacesTypes = {'cell', 'matrix'};
if iscell(faces)
    defaultFacesType = expectedFacesTypes{1};
else
    defaultFacesType = expectedFacesTypes{2};
end
parser.addParameter('facesType', defaultFacesType, ...
    @(x) any(validatestring(x, expectedFacesTypes)));

expectedOuterLoopTypes = {'faces', 'kSamples'};
if numel(faces) <= numel(kSamples) / 3
	defaultOuterLoopTypes = expectedOuterLoopTypes{1};
else
	defaultOuterLoopTypes = expectedOuterLoopTypes{2};
end
parser.addParameter('outerLoopType', defaultOuterLoopTypes, ...
    @(x) any(validatestring(x, expectedOuterLoopTypes)));

defaultGradients = [];
err = ['Optional input variable "gradients" should be either empty or ', ...
    'a 1d array with its length equal to the number of faces (size(faces, 1))'];
parser.addParameter('gradients', defaultGradients, ...
    @(x) assert(isempty(x) || ...
    (isnumeric(x) && any(size(x) == 1) && any(size(x) == size(faces, 1))), ...
    err));

err = 'Optional variable "verbose" should be either 0, 1, or 2';
parser.addParameter('verbose', 0, @(x) assert(x == 0 || x == 1 || x == 2, ...
    err));

parser.parse(faces, vertices, kSamples, args{:});

facesType = parser.Results.facesType;
type = parser.Results.type;
parallel = parser.Results.parallel;
outerLoopType = parser.Results.outerLoopType;
gradients = parser.Results.gradients;
verbose = parser.Results.verbose;

% Check for Parallel Computing Toolbox license (for use of parfor)
% this many not be necessary but parfor may be slower than for without PCT
if ~license('test', 'Distrib_Computing_Toolbox')
    options.parallel = false;
    warning('No Parallel Computing toolbox found. Reverting to standard code (slow)');
end

if ~isempty(gradients)
    if strcmpi(outerLoopType, 'kSamples')
        warning(['PolyFT with gradients does not support kSamples as ', ...
            'outer loop type currently; use faces as outer loop type ', ...
            'instead']);
        outerLoopType = 'faces';
    end
end

% -------------------------------------------------------------------------

function [kPoints, permMatrixRev, kSize] = shapeKPoints(kPoints)
% reshapes kPoints matrix into a Nx3 array.

kSize = size(kPoints);
kCoorsDim = boolean(zeros(size(kSize)));
kCoorsDim(find(kSize==3, 1)) = true;

% determine permutation matrix
permMatrix = 1:size(kSize,2);
permMatrix = [permMatrix(~kCoorsDim), permMatrix(kCoorsDim)];

% determine reverse permutation matrix and size (for use after FT)
permMatrixRev = 1:(size(kSize,2)-1);
permMatrixRev = [permMatrixRev(1:find(kCoorsDim)-1),...
    size(kSize,2), permMatrixRev(find(kCoorsDim):end)];

% reshape the KPoints matrix to Nx3 array 
kPoints = permute(kPoints, permMatrix);
kPoints = reshape(kPoints, [prod(kSize(~kCoorsDim)), 3]);

kSize(kCoorsDim) = 1;

% -------------------------------------------------------------------------

function FT = revShapeFT(FT, kSize, permMatrixRev)
% restores FT to match input kPoint dimensionality
FT= permute(reshape(FT, kSize), permMatrixRev);

% -------------------------------------------------------------------------

function faces = replicateFirstVertices(faces)

% replicates the first vertices in the face matrix or cell array to enclose
% the faces.

if iscell(faces) % faces is cell n by 1 cell array
    needRep = cellfun(@(x) x(1) ~= x(end), faces); 
    faces(needRep) = cellfun(@(x) [x, x(1)], faces(needRep), ...
        'UniformOutput', false);
else % faces is n by m matrix 
    if ~isequal(faces(:, 1), faces(:, end))
        faces = [faces, faces(:, 1)];
    end
end

% -------------------------------------------------------------------------

function faces = faceMatrix2Cell(faces)
% converts a faces matrix into a cell array. Every entry  
% of the cell is a row of vector of vertex indices for a given face. 

if ~iscell(faces)
    faces = mat2cell(faces, ones(1,size(faces, 1)), size(faces, 2)); 
end

% -------------------------------------------------------------------------

function faces = faceCell2Matrix(faces)
% converts a faces cell array into a number array. Every 
% row of the array is a list of vertex indices for a given face.

if iscell(faces)
    if size(faces, 1) == 1
        faces = faces.';
    end
    maxLength = max(cellfun(@(x) max(size(x(:))), faces));
    faces = cellfun(@(x) x(:)', faces , 'uniformoutput', false);
    faces = cellfun(@(x) [x, repmat(x(end), 1, maxLength - size(x, 2))], ...
        faces, 'uniformoutput', false);
    faces = cell2mat(faces);
end
