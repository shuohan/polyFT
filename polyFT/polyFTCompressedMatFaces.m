function FT = polyFTCompressedMatFaces(faces, vertices, kSamples, varargin)
%
% DESCRIPTION
%    optimizes polyFT calculation by using matrices instead of looping over
%    faces. The outmost loop is over kSamples and can be boosted by parfor.
%    The type of the input faces should be matrix. Direct usage is NOT
%    suggested. Call via polyFT.m instead.
%
% INPUT VARS
% - vertices: n x 3 matrix. Each row is the vector representing the 
%   coordinates of a vertex (3D).     
% - faces: f x m matrix. Each row represents the indices of the  
%   vertices that make up each of m-edge faces of the polyhedron.
% - kSamples: k space vectors, p x 3 matrix. Each row is the vector
%   representing the coordinates of a kspace sample.
%
% OUTPUT VARS
%    - FT: Fourier space data sampled at the coordinations of the kspace
%    samples.                      
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
% Daniel Herzka @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
% -------------------------------------------------------------------------

parallel = parseInputs(varargin);

faceNumber = size(faces, 1);
edgeNumber = size(faces, 2) - 1;

vertices = sortVertices(vertices, faces);
[edgeLengths, edges, edgeUnits] = calcEdges(vertices, faceNumber);
faceUnitNormals = calcFaceUnitNormals(edgeUnits, faceNumber);
edgeCenters = calcEdgeCenters(vertices, faceNumber);
edgeNormals = calcInPlaneEdgeNormals(edgeUnits, edgeLengths, faceUnitNormals, edgeNumber);
kSquaredNorms = getKSampleSquaredNorms(kSamples);

FT = complex(zeros(1, size(kSamples, 1)));

if parallel

    parfor i = 1 : size(kSamples, 1)

        kSample = kSamples(i, :);
        kSquaredNorm = kSquaredNorms(i, :);

        FT(i) = calcSingleKSampleFT(kSample, kSquaredNorm, edgeNormals, edges, ...
            faceUnitNormals, edgeCenters, vertices, faceNumber, edgeNumber);

    end

else

    for i = 1 : size(kSamples, 1)
        
        kSample = kSamples(i, :);
        kSquaredNorm = kSquaredNorms(i, :);

        FT(i) = calcSingleKSampleFT(kSample, kSquaredNorm, edgeNormals, edges, ...
            faceUnitNormals, edgeCenters, vertices, faceNumber, edgeNumber);

    end

end

% ------------------------------------------------------------------------

function parallel = parseInputs(args)

parser = inputParser;
parser.CaseSensitive = false;
parser.addParameter('parallel', true, @(x) isnumeric(x) || islogical(x)); 
parser.parse(args{:});
parallel = parser.Results.parallel;

% -------------------------------------------------------------------------

function vertices = sortVertices(vertices, faces)

% sorts and repliates the vertices to boost FT calculation.  
% The sorted vertices is in the following order:
% the first vertices from all faces, the second vertices from all faces, ...

vertices = vertices(reshape(faces, [1, numel(faces)]), :); 

% -------------------------------------------------------------------------

function [edgeLengths, edges, edgeUnits] = calcEdges(vertices, faceNumber)

% The size of the variable vertices is [faceNumber*edgeNumber, 3],
% generated from function sorteVertices.

% the second vertices from all faces minus the first vertices from all faces
edges = vertices(faceNumber + 1 : end, :) - vertices(1 : end - faceNumber, :);
edgeLengths = sqrt(sum(edges .* edges, 2));
edgeUnits = edges ./ repmat(edgeLengths, [1, size(edges, 2)]);

% -------------------------------------------------------------------------

function normals = calcFaceUnitNormals(edgeUnits, faceNumber)

firstEdges = edgeUnits(1 : faceNumber, :);

% select the second edges; use loop to find the second edges along the
% different directions
for i = faceNumber : faceNumber : size(edgeUnits, 1)
    secondEdges = edgeUnits((1 : faceNumber) + i, :);
    errors = abs(firstEdges - secondEdges);
    if sum(errors(:)) > 1e-10
        break
    end
end

tmp = cross(firstEdges, secondEdges, 2);
normals = tmp ./ repmat(sqrt(sum(tmp .^ 2, 2)), [1, size(tmp, 2)] );

% -------------------------------------------------------------------------

function edgeCenters = calcEdgeCenters(vertices, faceNumber)

edgeCenters = (vertices(faceNumber + 1 : end, :) ...
    + vertices(1 : end - faceNumber, :)) / 2;

% -------------------------------------------------------------------------

function normals = calcInPlaneEdgeNormals(edgeUnits, edgeLengths, ...
    faceNormals, edgeNumber)

% calculates the unit vectors orthogonal to edges and also in the plane of
% each face.

faceNormals = repmat(faceNormals, [edgeNumber, 1]);
normals = cross(edgeUnits, faceNormals, 2);
normals = repmat(edgeLengths, [1, size(normals, 2)]) .* normals;

% -------------------------------------------------------------------------

function squaredNorms = getKSampleSquaredNorms(kSamples)

squaredNorms = sum(kSamples .^ 2, 2);

% -------------------------------------------------------------------------

function FT = calcSingleKSampleFT(kSample, kSquaredNorm, edgeNormals, ...
    edges, faceNormals, edgeCenters, vertices, faceNumber, edgeNumber)

if sum(abs(kSample)) > 1e-10 % is not center of kspace
    
    [orthogonalIdx, otherIdx] = getConditionalFaceIdx(kSample, faceNormals);

    FT = calcOtherFaceFT(kSample, kSquaredNorm, faceNormals, ...
        edgeNormals, edges, edgeCenters, edgeNumber, otherIdx);

    FT = FT + calcOrthogonalFaceFT(edgeNumber, orthogonalIdx, ...
        faceNormals, vertices, edgeCenters, kSample, kSquaredNorm);

else

    FT = calcZeroKSampleFT(vertices, faceNumber, faceNormals, edgeNumber);

end

% ------------------------------------------------------------------------

function FT = calcZeroKSampleFT(vertices, faceNumber, faceNormals, edgeNumber)
    
vertices = vertices(1 : end - faceNumber, :);
faceAreas = calcFaceAreas(vertices, faceNormals, faceNumber, edgeNumber);
tmp = sum(vertices(1 : faceNumber, :) .* faceNormals, 2);
FT = 1 / 3 * sum(tmp .* faceAreas);

% -------------------------------------------------------------------------

function FT = calcOrthogonalFaceFT(edgeNumber, orthogonalIdx, ...
    faceNormals, vertices, edgeCenters, kSample, kSquaredNorm)

FT = 0;
selectedFaceNumber = sum(orthogonalIdx);

if selectedFaceNumber ~= 0
    
    repIdx = repmat(orthogonalIdx, [edgeNumber, 1]);
    
    faceAreas = calcFaceAreas(vertices(repIdx, :), faceNormals(orthogonalIdx, :), ...
        selectedFaceNumber, edgeNumber); 

    tmp = (faceNormals(orthogonalIdx, :) * kSample.') ...
       .* exp(-2 * pi * 1i * edgeCenters(orthogonalIdx, :) * kSample.')... 
       .* faceAreas; 
    FT = sum(tmp) * 1i / (2 * pi * kSquaredNorm);

end

% -------------------------------------------------------------------------

function FT = calcOtherFaceFT(kSample, kSquaredNorm, faceNormals, ...
    edgeNormals, edges, edgeCenters, edgeNumber, notOrthogonalIdx)

repIdx = repmat(notOrthogonalIdx, [edgeNumber, 1]);

faceNormals = faceNormals(notOrthogonalIdx, :);
edgeNormals = edgeNormals(repIdx, :);
edges = edges(repIdx, :);
edgeCenters = edgeCenters(repIdx, :);

FT = func1(kSample, kSquaredNorm, faceNormals) ...
    * func2(kSample, edgeNormals, edges, edgeCenters, edgeNumber) ...
    ./ -(4 * pi * pi * kSquaredNorm);

% -------------------------------------------------------------------------

function result = func1(kSample, kSquaredNorm, faceNormals)

% is partial internal calculation.

tmp = kSample * faceNormals.' ;
result = tmp ./ (kSquaredNorm - tmp .^ 2);

% -------------------------------------------------------------------------

function vec = func2(kSample, edgeNormals, edges, edgeCenters, edgeNumber)

% is partial internal calculation.
% has duplicated calculation for simplicity

faceNumber = size(edgeNormals, 1) / edgeNumber;

tmp1 = reshape(edgeNormals * kSample.', [faceNumber, edgeNumber]);
tmp2 = reshape(sinc(edges * kSample.'), [faceNumber, edgeNumber]);
tmp3 = reshape(exp(-2 * pi * 1i * (edgeCenters * kSample.')), ...
    [faceNumber, edgeNumber]);

vec = sum(tmp1 .* tmp2 .* tmp3, 2);    

% -------------------------------------------------------------------------
   
function faceAreas = calcFaceAreas(vertices, faceNormals, faceNumber, edgeNumber)

% has duplicated calculation for simplicity

secondVertices = [vertices(faceNumber + 1 : end, :); vertices(1 : faceNumber, :)];
dim =  size(vertices, 2);

tmp = cross(vertices, secondVertices, 2);
tmp = sum(reshape(tmp.', [dim, faceNumber, edgeNumber]), 3);
tmp = sum(faceNormals .* tmp.' , 2);

faceAreas = 0.5 * abs(tmp);

% -------------------------------------------------------------------------

function [orthogonalIdx, otherIdx] = getConditionalFaceIdx(kSample, faceNormals)

% calcualtes the indexes of faces which a k vector is ortogonal to and not
% orthogonal to.

zero = 1e-10;
otherIdx = sum(abs(faceNormals * skew(kSample)), 2) >  zero;
orthogonalIdx = ~otherIdx;
