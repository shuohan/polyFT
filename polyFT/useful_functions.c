#include "useful_functions.h"

const ptrdiff_t one = 1;
const ptrdiff_t dim = 3;
const double zero = 1e-10;

int parallel(double *vector1, double *vector2)
{
    int result;
    result = fabs(vector1[1] * vector2[2] - vector1[2] * vector2[1]) < EPS 
         && fabs(vector1[2] * vector2[0] - vector1[0] * vector2[2]) < EPS 
         && fabs(vector1[0] * vector2[1] - vector1[1] * vector2[0]) < EPS;
    return result;
}

void cross(double *vector1, double *vector2, double *result){ 
    result[0] = vector1[1] * vector2[2] - vector1[2] * vector2[1]; 
    result[1] = vector1[2] * vector2[0] - vector1[0] * vector2[2]; 
    result[2] = vector1[0] * vector2[1] - vector1[1] * vector2[0]; 
}

double sinc(double x){
    if (x == 0)
        return 1.0;
    else{
        x *= M_PI;
        return sin(x) / x;
    }
}

double calcFaceArea(double *vertices, size_t vertexNumber, double *face,
        size_t edgeNumber, double *faceNormal){

    size_t i, j;
    size_t idx1, idx2;
    double crossProduct[dim];
    double *vertex1, *vertex2;
    double vectorSum[DIM] = {0.0, 0.0, 0.0};
    double result;

    for (i = 0; i < edgeNumber; i++){
        idx1 = (size_t)(*(face + i));
        idx2 = (size_t)(*(face + i + 1));
        vertex1 = mxGetColumn(vertices, dim, idx1);
        vertex2 = mxGetColumn(vertices, dim, idx2);
        cross(vertex1, vertex2, crossProduct);
        for (j = 0; j < dim; j++){
            vectorSum[j] += crossProduct[j];
        }
    }
    result = fabs(0.5 * dot(faceNormal, vectorSum, dim));
    return result;
}

void calcFaceAreas(double *faceAreas, double *vertices, 
        size_t vertexNumber, double *faces, size_t faceNumber, 
        size_t edgeNumber, double *faceNormals)
{
    double *singleFace, *singleFaceNormal;
    size_t i;

    for (i = 0; i < faceNumber; i++){
        singleFace = mxGetColumn(faces, edgeNumber + 1, i);
        singleFaceNormal = mxGetColumn(faceNormals, dim, i);
        faceAreas[i] = calcFaceArea(vertices, vertexNumber, singleFace,
            edgeNumber, singleFaceNormal);
    }
}

double calcPolyVolume(double *faceAreas, double *vertices, double *faces,
    double *faceNormals, size_t faceNumber, size_t edgeNumber)
{
    double volume = 0;
    double *firstVertex, *singleFaceNormal;
    size_t vertexIdx;
    double *tmp;
    size_t i;

    for (i = 0; i < faceNumber; i++){
        tmp = mxGetColumn(faces, edgeNumber + 1, i);
        vertexIdx = (size_t)*tmp;
        firstVertex = mxGetColumn(vertices, dim, vertexIdx);
        singleFaceNormal = mxGetColumn(faceNormals, dim, i);
        volume += dot(firstVertex, singleFaceNormal, dim) * faceAreas[i];
    }
    volume /= 3;

    return volume;
}

double calcPolyVolumeCellFaces(double *faceAreas, double *vertices, 
    mxArray *faces, mxArray *faceNormals)
{
    double volume = 0;
    double *firstVertex;
    mwSize faceNumber;
    mwIndex f;
    mxArray *faceCell, *faceNormalCell;
    double *face, *faceNormal;

    faceNumber = mxGetNumberOfElements(faces);

    for (f = 0; f < faceNumber; f++)
    {
        faceCell = mxGetCell(faces, f);
        face = mxGetPr(faceCell);

        faceNormalCell = mxGetCell(faceNormals, f);
        faceNormal = mxGetPr(faceNormalCell);

        firstVertex = vertices + ((mwIndex)face[0]) * dim;
        volume += dot(firstVertex, faceNormal, dim) * faceAreas[f];
    }
    return volume / 3;
}

double **matrixAlloc(long rowNum, long colNum)
{
    long i, j;
    double **array; 
    
    array = (double **)malloc(sizeof(double *) * rowNum); 
    if (array == NULL){
        printf("ERROR: out of memory\n");
        return 0;
    }
    for (i = 0; i < rowNum; i++){
        array[i] = (double *)malloc(sizeof(double) * colNum);
        if (array[i] == NULL){
            printf("ERROR: out of memory\n");
            rowNum = i - 1;
            for (i = rowNum; i >= 0; i--)
                free(array[i]);
            return 0;
        }
    }
    return array;
}

void matrixFree(double **matrix, long rowNum)
{
    long i;
    for (i = 0; i < rowNum; i++)
        free((matrix)[i]);
    free(matrix);
}

void mexPrint2dArray(double *array, int firstLength, int secondLength){
    int i, j;
    int index;
    for (i = 0; i < firstLength; i++){
        for (j = 0; j < secondLength; j++){
            index = i * secondLength + j;
            mexPrintf("%f ", array[index]);
        }
        mexPrintf("\n");
    }
    mexPrintf("\n");
}

void mexPrint1dArray(double *array, int length){
    int i = 0;
    for (i = 0; i < length; i++){
        mexPrintf("%f ", array[i]);
    }
    mexPrintf("\n\n");
}
