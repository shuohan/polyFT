function intensities = assignIntensity(elements, vertices, intensityStepNum, ...
    varargin)

% assigns intensites to tetrahedral elements of the polyhedron volume mesh.
%
% INPUTS
% - elements: elementNum-by-4 matrix, tetrahedral elements forming the
%   volume mesh. Each row is the vertex index list.
% - vertices: vertexNum-by-3 matrix.
% - intensityStepNum: integer at least 1. The number of different
%   intensities assigned to this mesh
%
% OPTIONS
% - intensityMode: 'linear' (default) or 'spherical'; 'linear' when using
%   linear shaped intensity distribution, 'spherical' when using
%   ball-shaped intensity distribution.
% - intensityOriginLoc: 'center' (default), 'edge', or 'corner'; the
%   location where the lowest intensty is.
% 
% OUTPUT
% - intensities: 1-by-elementNum array
% 
% EXAMPLES
% intensities = assignIntensity(elements, vertices, intensityStepNum,
%   'intensityMode', 'spherical');
% intensities = assignIntensity(elements, vertices, intensityStepNum,
%   'intensityMode', 'linear', 'intensityOriginLoc', 'corner');
%
% ------------------------------------------------------------------------

[intensityMode, intensityOrigin, intensityStepNum] = parseInputs(...
    elements, vertices, intensityStepNum, varargin);

centers = getElementCenters(elements, vertices);
originRep = repmat(intensityOrigin, [size(centers, 1), 1]);
distanceVectors = centers - originRep;

if strcmpi(intensityMode, 'spherical')
    distances = sqrt(sum(distanceVectors .* distanceVectors, 2));
else
    direction = calcLinearIntensityDirection(intensityOrigin);
    dirRep = repmat(direction, [size(distanceVectors, 1), 1]);
    distances = dot(distanceVectors, dirRep, 2);
end

intensityTable = linspace(0.1, 1, intensityStepNum);
distances = rescale(distances, [1, intensityStepNum]);
intensities = intensityTable(round(distances));

% -------------------------------------------------------------------------

function [intensityMode, intensityOrigin, intensityStepNum] = parseInputs(...
    elements, vertices, intensityStepNum, options)

parser = inputParser;
parser.CaseSensitive = false;

parser.addRequired('elements', @(x) isnumeric(x) && ismatrix(x));
parser.addRequired('vertices', @(x) isnumeric(x) && ismatrix(x) ...
    && size(x, 2) == 3);
parser.addRequired('intensityStepNum', @(x) isscalar(x) && floor(x) == x)

expectedModes = {'linear', 'spherical'};
parser.addParameter('intensityMode', expectedModes{1}, ...
    @(x) any(validatestring(x, expectedModes)));
    
expectedLocs = {'center', 'edge', 'corner'};
parser.addParameter('intensityOriginLoc', expectedLocs{1}, ...
    @(x) any(validatestring(x, expectedLocs)));

parser.parse(elements, vertices, intensityStepNum, options{:});

intensityMode = parser.Results.intensityMode;
intensityOrigin = getOrigin(vertices, parser.Results.intensityOriginLoc);
intensityStepNum = min(intensityStepNum, size(elements, 1));

% -------------------------------------------------------------------------

function origin = getOrigin(vertices, originLoc)

xRange = [min(vertices(:, 1)), max(vertices(:, 1))];
yRange = [min(vertices(:, 2)), max(vertices(:, 2))];
zRange = [min(vertices(:, 3)), max(vertices(:, 3))];

if strcmpi(originLoc, 'center')
    origin = [sum(xRange), sum(yRange), sum(zRange)] / 2;
elseif strcmpi(originLoc, 'edge')
    origin = [xRange(1), sum(yRange) / 2, sum(zRange) / 2];
elseif strcmpi(originLoc, 'corner')
    origin = [xRange(1), yRange(1), zRange(1)];
end

% -------------------------------------------------------------------------

function direction = calcLinearIntensityDirection(origin)

refPoint = [0, 0, 0];
if isequal(origin, refPoint)
    direction = [0, 0, 1];
else
    direction = origin - refPoint;
    direction = direction / norm(direction);
end

% -------------------------------------------------------------------------

function result = rescale(input, targetRange)

originalRange = [min(input(:)), max(input(:))];
if diff(originalRange) < 1e-8
    result = originalRange(2) * ones(size(input));
else
    result = (input - originalRange(1)) / diff(originalRange) * ...
        diff(targetRange) + targetRange(1);
end

% -------------------------------------------------------------------------

function centers = getElementCenters(elements, vertices)

elementsTmp = elements(:);
verTmp = vertices(elementsTmp, :);

centersTmp = reshape(verTmp, [size(elements), size(verTmp, 2)]);
centersTmp = sum(centersTmp, 2) / size(centersTmp, 2);
centers = permute(centersTmp, [1, 3, 2]);
