/* calcuate the single face contribution of polyFT
 *
 * syntax: kspaceData = calcSingleFaceFTMex(face, vertices, kSamples)
 *
 * - face: contains the vertices indexes from the variable vertices of this
 *   face, the last vertex is the same with the first in face to enclose
 *   this face, 1-by-m, (m-1) is the number of the vertices of this face;
 *   ZERO BASED!!!!
 *
 * - vertices: the vertex vectors, p-by-3, p is the number of total
 *   vertices of the polyhedra 
 *
 * - kSamples: kspace point vectors, q-by-3, q is the number of kspace
 *   points
 *
 */

#include "useful_functions.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){

    // variables decaration
    
    #define faceInput prhs[0]
    #define verticesInput prhs[1]
    #define kspacePointsInput prhs[2]
    #define kspaceDataOutput plhs[0]

    mwIndex e, i, j, k, m, n; // loop counters
    mwSize edgeNumber, vertexNumber, kpointNumber;
    mwIndex verticesArrayNextIndex, verticesArrayNowIndex; 
    double tmp, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8;
    double area;
    double kpointNorm;
    double *kspacePoint;
    double *firstVertex;

    double *face; 
    double *vertices;
    double *kspacePoints;
    double *kspaceDataReal, *kspaceDataImag;

    double **edges; 
    double **edgeDirections;
    double **edgeNormals;
    double **edgeCenters;
    double *edgeLengths;
    double *faceNormal;

    // get input and output variables

    face = mxGetPr(faceInput);
    vertices = mxGetPr(verticesInput);
    kspacePoints = mxGetPr(kspacePointsInput);

    edgeNumber = mxGetN(faceInput) - 1; // the last vertex is duplicated
    vertexNumber = mxGetN(verticesInput);
    kpointNumber = mxGetN(kspacePointsInput);

    kspaceDataOutput = mxCreateDoubleMatrix(kpointNumber, 1, mxCOMPLEX);
    kspaceDataReal = mxGetPr(kspaceDataOutput);
    kspaceDataImag = mxGetPi(kspaceDataOutput);

    // calculate edge contribution
    
    // allocate edge arrays
    edges = matrixAlloc(edgeNumber, dim); 
    edgeDirections = matrixAlloc(edgeNumber, dim);
    edgeNormals = matrixAlloc(edgeNumber, dim);
    edgeCenters = matrixAlloc(edgeNumber, dim);

    faceNormal = malloc(sizeof(double) * dim);
    edgeLengths = malloc(sizeof(double) * edgeNumber);

    // calculate edges and edgeCenters
    for (e = 0; e < edgeNumber; e++)
        for (i = 0; i < dim; i++){
            verticesArrayNextIndex = (size_t)face[e+1] * dim + i;
            verticesArrayNowIndex = (size_t)face[e] * dim + i;
            edgeCenters[e][i] = (vertices[verticesArrayNextIndex] + 
                    vertices[verticesArrayNowIndex]) / 2;
            edges[e][i] = vertices[verticesArrayNextIndex] -  
                    vertices[verticesArrayNowIndex];
        }

    //calculate edgeLengths
    for (e = 0; e < edgeNumber; e++)
        edgeLengths[e] = twoNorm(edges[e], dim);

    // calculate edgeDirections
    for (e = 0; e < edgeNumber; e++)
        for (i = 0; i < dim; i++)
            edgeDirections[e][i] = edges[e][i] / edgeLengths[e];

    // calculate faceNormal
    cross(edgeDirections[0], edgeDirections[1], faceNormal); 
    tmp = twoNorm(faceNormal, dim);
    divideScaler(faceNormal, dim, tmp);

    // calculate edge normals
    for (e = 0; e < edgeNumber; e++){
        cross(edgeDirections[e], faceNormal, edgeNormals[e]);
    }
    
    // calculate the face contribution to the FT

    firstVertex = mxGetColumn(vertices, dim, (size_t)face[0]);
    area = calcFaceArea(vertices, vertexNumber, face, edgeNumber, faceNormal);

    for (i = 0; i < kpointNumber; i++){

        kspacePoint = mxGetColumn(kspacePoints, dim, i);
        kpointNorm = twoNorm(kspacePoint, dim);

        if (kpointNorm < EPS){ // is at the center of the kspace

            kspaceDataReal[i] += dot(firstVertex, faceNormal, dim) * area / 3;
            
        }else if (parallel(kspacePoint, faceNormal)){ 

            tmp1 = M_PI * dot(kspacePoint, faceNormal, dim);
            tmp2 = -2 * M_PI * dot(kspacePoint, firstVertex, dim);
            tmp3 = 2 * area;
            tmp4 = 2 * M_PI * kpointNorm;
            tmp4 = tmp4 * tmp4;
            tmp5 = tmp3 * tmp1 / tmp4;

            kspaceDataReal[i] -= sin(tmp2) * tmp5;
            kspaceDataImag[i] += cos(tmp2) * tmp5;
                
        }else{ 

            tmp1 = 0;
            tmp2 = 0;

            for (e = 0; e < edgeNumber; e++){

                tmp3 = dot(kspacePoint, edgeNormals[e], dim);
                tmp4 = sinc(dot(kspacePoint, edgeDirections[e], dim) 
                        * edgeLengths[e]);
                tmp5 = -2 * M_PI * dot(kspacePoint, edgeCenters[e], dim);
                tmp6 = edgeLengths[e] * tmp3 * tmp4;

                tmp1 += tmp6 * cos(tmp5);
                tmp2 += tmp6 * sin(tmp5);
            }

            tmp3 = dot(kspacePoint, faceNormal, dim);
            tmp4 = kpointNorm * kpointNorm;
            tmp5 = tmp3 * tmp3;
            tmp6 = 2 * M_PI * kpointNorm;
            tmp7 = -tmp3 / (tmp4 - tmp5) / tmp6 / tmp6 ;

            kspaceDataReal[i] += tmp7 * tmp1;
            kspaceDataImag[i] += tmp7 * tmp2;
        }
    }

    matrixFree(edges, edgeNumber); 
    matrixFree(edgeDirections, edgeNumber);
    matrixFree(edgeNormals, edgeNumber);
    matrixFree(edgeCenters, edgeNumber);
    free(edgeLengths);
    free(faceNormal);
}
