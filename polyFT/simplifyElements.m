function [faces, gradients] = simplifyElements(elements, vertices, intensities)

% simpifies the mesh. First extracts faces of all the elements. Then makes
% sure the normals of all faces are outwards. Finally removes faces with
% the same intensities at both sides, removes duplicated faces, and
% calcualtes the gradients of intensities (inside intensty - outside
% intensity) of the rest faces.
%
% INPUTS
% - elements: elementNum-by-4 matrix
% - vertices: vertexNum-by-3 matrix
% - intensties: 1-by-elementNum array; each item is the intensities
%   assigned to the corresponding element.
% 
% OUTPUTS
% - faces: faceNum-by-3 matrix
% - gradients: 1-by-faceNum array. Each item is the gradients (inside
%   outside intensity differences)
%
% ------------------------------------------------------------------------

faces = getFacesFromTetrahedrons(elements, vertices);
[faces, gradients] = calcFaceIntensityGradients(faces, intensities);

% -------------------------------------------------------------------------

function faces = getFacesFromTetrahedrons(elements, vertices)

faceNumPerElement = 4;
elementSize = size(elements);

colIdx = [1 2 3; 2 3 4; 3 4 1; 4 1 2];
idx = getIdxOfElements(colIdx, elementSize, faceNumPerElement);

elements = elements(:);
faces = elements(idx);

colIdx = [4; 1; 2; 3];
idx = getIdxOfElements(colIdx, elementSize, faceNumPerElement);
theFourthVertexIdx = elements(idx);

firstVertices = vertices(faces(:, 1), :);
secondVertices = vertices(faces(:, 2), :);
thirdVertices = vertices(faces(:, 3), :);

firstEdges = secondVertices - firstVertices;
secondEdges = thirdVertices - secondVertices;
faceNormals = cross(firstEdges, secondEdges, 2);

outwardVectors = firstVertices - vertices(theFourthVertexIdx, :);

projections = dot(outwardVectors, faceNormals, 2);
reversedFaceIdx = projections < 0;
illFaceIdx = projections == 0;

faces(reversedFaceIdx, :) = faces(reversedFaceIdx, end : -1 : 1, :);
faces(illFaceIdx, :) = [];


function idx = getIdxOfElements(faceIdx, elementSize, faceNumPerElement)

colIdx = repmat(faceIdx, [elementSize(1), 1]);
tmpIdx = repmat(1 : elementSize(1), [faceNumPerElement, 1]);
rowIdx = repmat(tmpIdx(:), [1, size(colIdx, 2)]);
idx = sub2ind(elementSize, rowIdx, colIdx);

% -------------------------------------------------------------------------

function [faces, gradients] = calcFaceIntensityGradients(faces, intensities)

% - faces: has the following order:
%
%                 - face 1 (vertex 11, vertex 12, vertex 13)
%   tetrahedron 1 | face 2 (vertex 21, vertex 22, vertex 23)
%                 | face 3 (vertex 31, vertex 32, vertex 33)
%                 - face 4 (vertex 41, vertex 42, vertex 43)
%                 - face 5 (vertex 51, vertex 52, vertex 53)
%   tetrahedron 2 | face 6 (vertex 61, vertex 62, vertex 63)
%                 | face 7 (vertex 71, vertex 72, vertex 73)
%                 - face 8 (vertex 81, vertex 82, vertex 83)
%   ...                     
% 
% - intensities: the intensities of elements

%% assign face intensities

% every face has an intensity with the value equal to the intensity within 
% the tetrahedron that the face encloses of.

intensities = reshape(intensities, [1, length(intensities)]);
intensities = repmat(intensities, [4, 1]);
intensities = reshape(intensities, [numel(intensities), 1]);

%% get the indexes of shared faces

% assign an ID to each face; shared faces have the same IDs
sortedFaces = sort(faces, 2);
[~, ~, ids] = unique(sortedFaces, 'rows'); 

% sort the IDs. If the difference between two consecutive sorted IDs is 0,
% the faces with these two IDs are shared faces.
[sortedIDs, order] = sort(ids);
diffIDs = diff(sortedIDs); 
sharedFaceIdxTmp = abs(diffIDs) < 1e-8;
firstSharedFaceIdx = [sharedFaceIdxTmp; false];
secondSharedFaceIdx = [false; sharedFaceIdxTmp];

% each row of sharedFacePair is one pair of shared faces
sharedFacePairs = [order(firstSharedFaceIdx), order(secondSharedFaceIdx)];

%% find 3 sets of indexes: 

% (1) polyhedral boundary faces (not shared faces) 
% (2) shared faces with the same intensity
% (3) shared faces with different intensity

sharedFaceIntensityDifferences = intensities(sharedFacePairs(:, 1)) ...
    - intensities(sharedFacePairs(:, 2));

% (2) shared faces with the same intensity
% idxtmp2 = sharedFaceIntensityDifferences == 0;
% idx2 = sharedFacePairs(idxtmp2, :);

% (3) shared faces with different intensity
idxtmp3 = sharedFaceIntensityDifferences ~= 0;
idx3 = sharedFacePairs(idxtmp3, :);

% (1) polyhedral boundary faces (not shared faces) 
boundaryFaces = order(~(firstSharedFaceIdx | secondSharedFaceIdx));

% delete shared faces with the same intensities and ones of the shared face
% pairs with different with different intensities, then reassign the face
% intensities

keptFaceIdx = [boundaryFaces; idx3(:, 1)];
faces = faces(keptFaceIdx, :);

gradients = intensities;
gradients(idx3(:, 1)) = sharedFaceIntensityDifferences(idxtmp3);
gradients = gradients(keptFaceIdx);
