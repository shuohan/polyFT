function FT = polyFTCompressedKSamples(faces, vertices, kSamples, varargin)
%
% DESCRIPTION
% optimizes polyFT calculation by using matrices instead of looping over
% kSamples. The outmost loop is over faces and can be boosted by
% parfor. Direct usage is NOT suggested.
%
% INPUT VARIABLES
% - vertices: n x 3 matrix. Each row is the vector representing the 
%   coordinates of a vertex (3D).     
% - faces: m x 1 number array. Each entry represents the indices of the  
%   vertices that make up each of m faces of the polygon.
% - kSamples: k space samples, p x 3 matrix. Each row is the k point 
%   vector.  
%
% OPTION
% - parallel: if use parallel computing
%
% OUTPUT VARIABLES
% - FT: Fourier space data sampled at the coordinations of kspace samples.                      
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
% Daniel Herzka @JHU-SOM
%  Dynamic Medical Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
% -------------------------------------------------------------------------

[parallel, gradients] = parseInputs(varargin, size(faces, 1));

FT = complex(zeros(size(kSamples, 1), 1));

if ~iscell(faces)

    if parallel
        if ~isempty(gradients)
            parfor i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces(i, :), :), kSamples) ...
                    * gradients(i);
            end
        else
            parfor i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces(i, :), :), kSamples);
            end
        end
    else
        if ~isempty(gradients)
            for i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces(i, :), :), kSamples) ...
                    * gradients(i);
            end
        else
            for i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces(i, :), :), kSamples);
            end
        end
    end
else

    if parallel
        if ~isempty(gradients)
            parfor i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces{i}, :), kSamples)...
                    * gradients(i);
            end
        else
            parfor i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces{i}, :), kSamples);
            end
        end
    else
        if ~isempty(gradients)
            for i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces{i}, :), kSamples)...
                    * gradients(i);
            end
        else
            for i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFT(vertices(faces{i}, :), kSamples);
            end
        end
    end

end

% ------------------------------------------------------------------------

function [parallel, gradients] = parseInputs(args, faceNumber)

parser = inputParser;
parser.CaseSensitive = false;

% parallel
parser.addParameter('parallel', true, @(x) isnumeric(x) || islogical(x)); 

% gradients
defaultGradients = [];
parser.addParameter('gradients', defaultGradients, ...
    @(x) isempty(x) || ...
    (isnumeric(x) && any(size(x) == 1) && any(size(x) == faceNumber)));

parser.parse(args{:});
parallel = parser.Results.parallel;
gradients = parser.Results.gradients;

% ------------------------------------------------------------------------

function FT = calcSingleFaceFT(vertices, kSamples)

% calcuates the FT contribution of the target face to the polyhedron FT
%
% There are three different conditions of the FT contribution according to
% the spatial relationships between the target face and the target kspace
% samples:
% 1. zero: the kspace sample is at the origin (0, 0, 0) of kspace,
% 2. orthogonal: the kspace samples are orthogonal to the face,
% 3. others.
% These three conditions have different equations, so they need to be
% calcualted seperately.

FT = complex(zeros(size(kSamples, 1), 1));

[edgeLengths, edges, edgeUnits] = calcEdges(vertices); 
edgeCenters = calcEdgeCenters(vertices);
faceUnitNormal = calcFaceUnitNormal(edgeUnits);
edgeUnitNormals = calcInPlaneEdgeUnitNormals(edgeUnits, faceUnitNormal);
faceAreas = calcFaceAreas(vertices, faceUnitNormal);

[zeroIdx, orthogonalIdx, otherIdx] = getConditionalIdx(kSamples, faceUnitNormal);

FT(zeroIdx) = calcZeroKSampleFT(vertices, faceUnitNormal, faceAreas);

otherKSamples = kSamples(otherIdx, :);
FT(otherIdx) = calcOtherKSamplesFT(otherKSamples, vertices, ...
    edges, edgeLengths, edgeUnitNormals, edgeCenters, faceUnitNormal);

orthogonalKSamples = kSamples(orthogonalIdx, :);
FT(orthogonalIdx) = calcOrthogonalKSamplesFT(orthogonalKSamples, ...
    vertices, faceUnitNormal, faceAreas);

% -------------------------------------------------------------------------

function [edgeLengths, edges, edgeUnits] = calcEdges(vertices)

% edgeUnits: the unit vectors along the same directions of edges

edges = vertices(2 : end, :) - vertices(1 : end-1, :);
edgeLengths = sqrt(sum(edges .* edges, 2));
edgeUnits = edges ./ repmat(edgeLengths, [1, size(edges, 2)]);

% -------------------------------------------------------------------------

function centers = calcEdgeCenters(vertices)

centers = (vertices(1 : end - 1, :) + vertices(2 : end, :)) / 2;

% -------------------------------------------------------------------------

function normal = calcFaceUnitNormal(edgeUnits)

tmp = cross(edgeUnits(1, :), edgeUnits(2, :));
normal = tmp ./ norm(tmp);

% -------------------------------------------------------------------------

function normals = calcInPlaneEdgeUnitNormals(edgeUnits, faceUnitNormal)

% calculates the unit vectors orthogonal to edges of the target face and
% also in the target face plane.
% "Skew matrix" is used to calculate cross produts fast.

normals = edgeUnits * skew(faceUnitNormal);

% -------------------------------------------------------------------------

function faceAreas = calcFaceAreas(vertices, faceUnitNormal)

tmp = cross(vertices(2:end, :), vertices(1:end-1, :), 2); 
faceAreas = 0.5 * abs(sum(tmp, 1) * faceUnitNormal.');

% -------------------------------------------------------------------------

function FT = calcZeroKSampleFT(vertices, faceUnitNormal, faceAreas)

FT = 1/3 * vertices(1, :) * faceUnitNormal.' * faceAreas;

% ------------------------------------------------------------------------

function FT = calcOtherKSamplesFT(kSamples, vertices, edges, edgeLengths, ...
    edgeUnitNormals, centers, faceUnitNormal)

kSquaredNorms = sum(kSamples .^ 2, 2);
tmp1 = repmat(edgeLengths, [1, length(faceUnitNormal)]) .* edgeUnitNormals; 
tmp2 = kSamples * faceUnitNormal.';

FT = tmp2 ./ (kSquaredNorms - tmp2 .^ 2 ) ...
    ./ (-(2 * pi) ^ 2 .* kSquaredNorms) ...
    .* sum(sinc(kSamples * edges.') ...
           .* exp(-2 * pi * 1i * kSamples * centers') ...
           .* (kSamples * tmp1.'),  2);

% -------------------------------------------------------------------------

function FT = calcOrthogonalKSamplesFT(kSamples, vertices, faceUnitNormal, faceAreas)

% the constant can be eliminated 

kSquaredNorms = sum(kSamples .^ 2, 2);

FT = -2 * pi * 1i * (kSamples * faceUnitNormal.') ...
    ./ (-(2 * pi) ^ 2 .* kSquaredNorms) ...
    .* exp(-2 * pi * 1i * kSamples * vertices(1, :).') .* faceAreas;

% -------------------------------------------------------------------------

function [zeroIdx, orthogonalIdx, otherIdx] = getConditionalIdx(kSamples, faceUnitNormal)

zero = 1e-10;

zeroIdx = sum(abs(kSamples), 2) < zero;
otherIdx = sum(abs(kSamples * skew(faceUnitNormal)), 2) >  zero;
orthogonalIdx = ~otherIdx;
orthogonalIdx(zeroIdx) = false;
