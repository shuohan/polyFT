#include "mex.h"
#include "blas.h"
#include <math.h>
#include <stdio.h>

#define M_PI 3.14159265358979323846264338327950288
#define DIM 3
#define EPS 0.00000001 // small number
extern const ptrdiff_t one;
extern const ptrdiff_t dim;
extern const double zero;

// BLAS functions

#define twoNorm(vector, len) dnrm2(&len, vector, &one)
#define oneNorm(vector, len) dasum(&len, vector, &one)
#define dot(vector1, vector2, len) ddot(&len, vector1, &one, vector2, &one)
#define timeScaler(vector, len, scaler) dscal(&len, &scaler, vector, &one)
#define divideScaler(vector, len, scaler) \
    (scaler = 1.0/scaler, timeScaler(vector, len, scaler))

// math functions

int parallel(double *vector1, double *vector2);
void cross(double *vector1, double *vector2, double *result);
double sinc(double x);
double calcFaceArea(double *vertices, size_t vertexNumber, double *face,
        size_t edgeNumber, double *faceNormal);
void calcFaceAreas(double *faceAreas, double *vertices, 
        size_t vertexNumber, double *faces, size_t faceNumber, 
        size_t edgeNumber, double *faceNormals);
double calcPolyVolume(double *faceAreas, double *vertices, double *faces,
        double *faceNormals, size_t faceNumber, size_t edgeNumber);
double calcPolyVolumeCellFaces(double *faceAreas, double *vertices, 
    mxArray *faces, mxArray *faceNormals);

// mex functions

#define mxGetColumn(matrix, rowNum, colIdx) matrix + (colIdx) * (rowNum)
void mexPrint2dArray(double *array, int firstLength, int secondLength);
void mexPrint1dArray(double *array, int length);

// others

double **matrixAlloc(long rowNum, long colNum);
void matrixFree(double **matrix, long rowNum);
