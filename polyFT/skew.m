function mat = skew(vect)

% using skew matrix to perform cross product between a vector with a matrix
% of vectors can increase the time effciency.

mat = [0, -vect(3), vect(2);
       vect(3), 0, -vect(1);
       -vect(2), vect(1), 0];
