/* calcuate the single kspace sample polyFT
 *
 * syntax: 
 *
 * FT = calcSingleKSampleFTMex(kSample, edges, edgeUnits,
 *     edgeCenters, edgeNormals, faceNormals, faceAreas, volume)
 *
 * INPUT VARIABLES
 * - kSample: 1 by 3 matrix.
 * - others: generated from function prepareForSingleKSample.c
 *
 * OUTPUT VARIABLE
 * - FT: a complex number, the FT at this k sample
 *
 */

#include "useful_functions.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){

    // variables decaration

    #define kSampleInput prhs[0]
    #define edgesInput prhs[1]
    #define edgeUnitsInput prhs[2]
    #define edgeCentersInput prhs[3]
    #define edgeNormalsInput prhs[4]
    #define faceNormalsInput prhs[5]
    #define faceAreasInput prhs[6]
    #define volumeInput prhs[7]

    #define FTOutput plhs[0]

    // input sizes
    mwSize faceNumber, edgeNumber;
    // cell elememts of inputs
    mxArray *edgesCell, *edgeCentersCell, *edgeLengthsCell, *edgeUnitsCell,
            *faceNormalCell, *edgeNormalsCell;
    // matricies
    double *edges, *edgeCenters, *edgeUnits, *faceNormal,
           *edgeNormals, *faceAreas;
    // columes of matricies
    double *edge, *edgeCenter, *edgeNormal, *firstVertex, *kSample;
    // outputs
    double *FTReal, *FTImag;
    double edgeLength, kSquaredNorm;
    // others
    double tmp, tmp1, tmp2, tmp3, tmp4;
    mwIndex f, e, i, j;

    faceNumber = mxGetNumberOfElements(edgesInput);
    kSample = mxGetPr(kSampleInput);

    FTOutput = mxCreateDoubleMatrix(1, 1, mxCOMPLEX);
    FTReal = mxGetPr(FTOutput);
    FTImag = mxGetPi(FTOutput);

    tmp = twoNorm(kSample, dim);

    if (tmp <= EPS)
    {
        *FTReal = *(mxGetPr(volumeInput));
    }
    else
    {
        *FTReal = 0;
        *FTImag = 0;

        faceAreas = mxGetPr(faceAreasInput);
        kSquaredNorm = tmp * tmp;

        for (f = 0; f < faceNumber; f++)
        {
            faceNormalCell = mxGetCell(faceNormalsInput, f);
            faceNormal = mxGetPr(faceNormalCell);

            edgeCentersCell = mxGetCell(edgeCentersInput, f);
            edgeCenters = mxGetPr(edgeCentersCell);

            if (parallel(kSample, faceNormal))
            {
                firstVertex = mxGetColumn(edgeCenters, dim, 0);

                tmp1 = 2 * M_PI * dot(kSample, faceNormal, dim) * faceAreas[f];
                tmp2 = 2 * M_PI * dot(kSample, firstVertex, dim);

                *FTReal += -tmp1 * sin(tmp2);
                *FTImag += -tmp1 * cos(tmp2);
            }
            else
            {
                edgesCell = mxGetCell(edgesInput, f);
                edges = mxGetPr(edgesCell);

                edgeUnitsCell = mxGetCell(edgeUnitsInput, f);
                edgeUnits = mxGetPr(edgeUnitsCell);

                edgeNormalsCell = mxGetCell(edgeNormalsInput, f);
                edgeNormals = mxGetPr(edgeNormalsCell);

                edgeNumber = mxGetN(edgesCell);

                tmp3 = 0;
                tmp4 = 0;

                for (e = 0; e < edgeNumber; e++)
                {
                    edgeNormal = edgeNormals + dim * e;
                    edge = edges + dim * e;
                    edgeCenter = edgeCenters + dim * e;
                    tmp1 = dot(kSample, edgeNormal, dim) 
                        * sinc(dot(kSample, edge, dim));
                    tmp2 = -2 * M_PI * dot(kSample, edgeCenter, dim);
                    tmp3 += tmp1 * cos(tmp2);
                    tmp4 += tmp1 * sin(tmp2);
                }
                tmp1 = dot(kSample, faceNormal, dim);
                tmp2 = tmp1 / (kSquaredNorm - tmp1 * tmp1);
                *FTReal += tmp2 * tmp3;
                *FTImag += tmp2 * tmp4;
            }
        }
        tmp = -1 / (4 * M_PI * M_PI * kSquaredNorm);
        *FTReal = *FTReal * tmp;
        *FTImag = *FTImag * tmp;
    }
}
