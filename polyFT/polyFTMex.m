function FT = polyFTMex(faces, vertices, kSamples, varargin)

% calculates FT using mex functions. Directly calling is NOT suggested.
% Call by polyFT(..., 'mex', true) instead.
%
% INPUT VARIABLES
% - faces: cell or matrix; size(faces, 1) is the faceNumber; the first
% vertex of each face should be duplicated.
% - vertices: vertexNumber-by-3 matrix
% - kSamples: kSampleNumber-by-3 matrix, the location vectors where FT is
%   computed; 
% Options:
% - outerLoopType: "faces" when using calcSingleFaceMex; "kSamples" whne
%   using calcSingleKSampleMex; (capitalization) case insensitive 
% - parallel: true: using parfor from matlab parallel computing toolbox;
%   false: not using
%
% OUTPUT VARIABLES
% - FT: has the same length with kSampleNumber.
%
% EXAMPLES:
% 1. use parfor:
%   FT = polyFTMex(faces, vertices, kSamples, 'parallel', true);
% 2. outerLoop is faces:
%   FT = polyFTMex(faces, vertices, kSamples, 'outerLoopType', 'faces');
%

[outerLoopType, parallel, gradients] = parseInputs(varargin, size(faces, 1));

FT = complex(zeros(size(kSamples, 1), 1));

% convert to row major matrix to facilitate mex computation
vertices = vertices.'; 
kSamples = kSamples.';

faces = convertFaces(faces);

if parallel 

    if ~isempty(gradients)
        
        parfor i = 1 : size(faces, 1)
            FT = FT + calcSingleFaceFTMex(faces{i}, vertices, kSamples)...
                * gradients(i);
        end
        
    else
        
        if strcmpi(outerLoopType, 'faces')

            parfor i = 1 : size(faces, 1)
                FT = FT + calcSingleFaceFTMex(faces{i}, vertices, kSamples);
            end

        else

            [edges, edgeLengths, edgeUnits, edgeCenters, edgeNormals, ...
                faceNormals, faceAreas, volume] = ...
                prepareForSingleKSample(faces, vertices);

            parfor i = 1 : size(kSamples, 2)
                FT(i) = calcSingleKSampleFTMex(kSamples(:, i), edges, ...
                    edgeUnits, edgeCenters, edgeNormals, ...
                    faceNormals, faceAreas, volume);
            end

        end
        
    end

else
    % implement mex performing all computation in the future; 
    % use calcSingleFaceFTMex for now.
    
    if ~isempty(gradients)
        for i = 1 : size(faces, 1)
            FT = FT + calcSingleFaceFTMex(faces{i}, vertices, kSamples) ...
                * gradients(i);
        end
    else
        for i = 1 : size(faces, 1)
            FT = FT + calcSingleFaceFTMex(faces{i}, vertices, kSamples);
        end
    end
    
end

% -------------------------------------------------------------------------

function [outerLoopType, parallel, gradients] = parseInputs(args, faceNumber)

parser = inputParser;
parser.CaseSensitive = false;

% outerLoopType
expectedOuterLoopTypes = {'faces', 'kSamples'};
defaultOuterLoopType = expectedOuterLoopTypes{2};
parser.addParameter('outerLoopType', defaultOuterLoopType, ...
    @(x) any(validatestring(x, expectedOuterLoopTypes)));

% parallel
parser.addParameter('parallel', true, @(x) isnumeric(x) || islogical(x)); 

% gradients
defaultGradients = [];
parser.addParameter('gradients', defaultGradients, ...
    @(x) isempty(x) || ...
    (isnumeric(x) && any(size(x) == 1) && any(size(x) == faceNumber)));

parser.parse(args{:});
outerLoopType = parser.Results.outerLoopType;
parallel = parser.Results.parallel;
gradients = parser.Results.gradients;

% -------------------------------------------------------------------------

function faces = convertFaces(faces)

if ~iscell(faces)
    % 0 based indexing in c funtion
    faces = faces - 1;
    faces = mat2cell(faces, ones(size(faces, 1), 1), size(faces, 2));
else
    % 0 based indexing in c funtion
    faces = cellfun(@(x) x - 1, faces, 'UniformOutput', false);
end
