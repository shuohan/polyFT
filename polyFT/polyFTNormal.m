function kdata = polyFTNormal( faces, vertices, kPoints )
% 
% General function that directly follows the equations of polyhedral FT, as
% given by Kormska e Kormska J. (1988, Optik: 80, 171-183. Algebraic 
% expressions of shape amplitudes of polygons and polyhedra) and repeated 
% in Ngo TM et al. (2015,  Magnetic Resonance in Medicine,  
% Realistic Analytical Polyhedral MRI Phantoms). Assumes that all faces of
% polyhedron have the same number of vertices (e.g. triangular meshes).
% Inputs follow the face-vertex representation of meshes. Equations use
% for loops with no vectorization or parallelization and can be quite 
% slow. <DNU>
%
% INPUT VARS
% - faces: m-by-p matrix. Each row in the array represents a list of 
%   vertex indices. m=number of faces in the mesh. p=number of 
%   vertices making up the faces. For triangular meshes p=3.
% - vertices: n-by-3 matrix. Each row is the coordinate of this
%   vertex. n=number of vertices in the mesh.
% - kPoints: k-by-3 matrix; each row is the coordinate of this sample
%
%
% -------------------------------------------------------------------------
% Shuo Han @JHU-SOM
% Tri Ngo @JHU-SOM
%  Dynamic Imaging Laboratory
%  Department of Biomedical Engineering, 
%  Johns Hopkins University School of Medicine
%  Baltimore, MD
% -------------------------------------------------------------------------

F = size(faces, 1);  % the number of faces

%% allocate output
kdata = complex(zeros(size(kPoints, 1), 1));

%% calculate associated parameters

for f = 1 : F
    
    %fprintf('Computing face # %d\n', f);
    
    E = length(faces{f}) - 1;   % edges number per face; equal to 
                                    % the number of vertices - 1
    L = zeros(E, 1);   % edges length
    t = zeros(E, 3);   % unit vector of each edge's direction vector
    n = zeros(E, 3);   % unit vector vertical to the edge and in this plane
    r_c = zeros(E, 3); % center points coordinates of each edge
    
    for e = 1 : E
        
        L_hat = vertices(faces{f}(e + 1), :) - vertices(faces{f}(e), :); 
            % the vector (the edge) between two vertives of a face
            
        L(e) = norm(L_hat);
        t(e, :) = L_hat / L(e); 
        r_c(e, :) = vertices(faces{f}(e), :) + ...
                      t(e, :) * L(e) / 2; 
    
    end
    
    t(isnan(t)) = 0;
    r_c(isnan(r_c)) = 0;
    
    N_f = cross(t(1, :), t(2, :)) / norm(cross(t(1, :), t(2, :))); 
        % the direction unit vector of this face
        
    for e = 1 : E
        
        n(e, :) = cross(t(e, :), N_f);
        
    end
    
    %calculate appropriate face contribution
    for i = 1 : size(kPoints, 1)
        
        k = kPoints(i, :);
        
        if norm(k) < 1e-6 % center of k-space
            
            %volume of polyhedron
            a = [0 0 0];
            
            for e = 1 : E
                
                a = a + cross(vertices(faces{f}(e), :), ...
                    vertices(faces{f}(e + 1), :));
                
            end
            
            kdata(i) = kdata(i) + dot(vertices(faces{f}(1), :), N_f) ...
                * norm(dot(N_f, a));
            
        else % check to see if vectors are almost perpenicular, 
             % notice that k and N_f are unit length so the length 
             % of crossproduct is the sine of angle between them
            
            if norm(cross(k, N_f)) < 1e-6
                
                a = 0;
                
                % k perpedicular to plane of face
                for e = 1 : E
                    
                    a = a + cross(vertices(faces{f}(e), :), ...
                        vertices(faces{f}(e + 1), :));
                    
                end
                
                % note below that factor of two is gone because expression 
                % for P_f contains (1 / 2) which cancels it out
                
                kdata(i) = kdata(i) - 1i * pi * dot(k, N_f) *...
                    norm(dot(N_f, a)) * ...
                    exp( -pi * 2i * dot( k, vertices(faces{f}(1), :) ) );
                
            else % the usual contribution
      
                b = 0;
                
                for e = 1 : length(faces{f}) - 1
                    
                    b = b + L(e) * dot(k, n(e, :)) * ...
                        sinc(dot(k, t(e, :)) * L(e)) * ...
                        exp( -pi * 2i * dot( k, r_c(e, :) ) );
                    
                end
                
                kdata(i) = kdata(i) + ...
                    (dot(k, N_f) / (norm(k)^2 - dot(k, N_f)^2)) * b;
                
            end
            
        end
        
    end
    
end

%% multiply with appropriate coefficient

for i = 1 : size(kPoints, 1)
    
    k = kPoints(i, :);
    
    if norm(k) > 1e-6
        kdata(i) = - kdata(i) / (2 * pi * norm(k)) ^ 2;
    else
        kdata(i) = kdata(i) / 6;
    end
    
end
