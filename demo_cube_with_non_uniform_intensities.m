polyFTAddPath;

%% set parameters 

SHOW_MESH = true;
SHOW_RECON = true;

Nx = 2 ^ 4;
Ny = Nx;
Nz = Nx;

vertexNumPerEdge = 10;

%% get mesh

[elements, vertices] = getCubeVolumeMesh(vertexNumPerEdge);
intensities = assignIntensity(elements, vertices, 10, ...
    'intensityMode', 'spherical', 'intensityOrigin', 'edge');
[faces, gradients] = simplifyElements(elements, vertices, intensities);

if SHOW_MESH
    showMesh(faces, vertices);
end

%% polyFT

[Lx, Ly, Lz] = getFOV(vertices);

kSamples = getUniformKsamples(Nx, Ny, Nz, Lx, Ly, Lz);
imageSamples = getImageSpaceSamples(Nx, Ny, Nz, Lx, Ly, Lz);

tic
FT = polyFT(faces, vertices, kSamples, 'Type', 'mex', 'Parallel', true, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'Gradients', gradients);
t = toc;

printTimingInfo(t, size(faces, 1), numel(kSamples) / 3);

%% reconstruct

I = cartesianRecon(FT, Nx, Ny, Nz, Lx, Ly, Lz);

if SHOW_RECON
    figure,
    showRecon(I, imageSamples, min(intensities) * 0.5); 
end
