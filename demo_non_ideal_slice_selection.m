polyFTAddPath;

%% set parameters 

SHOW_MESH = true;
SHOW_RECON = true;

kSampleNumX = 2 ^ 6;
kSampleNumY = kSampleNumX;

% plane parameters
thickness = 0.1;
point = [0 0 0];
normal = [0 0 1];

profile = permute(hanning(100), [3, 2, 1]);

%% get mesh slice

% ideal slice selection (without selection profile)
[faces1, vertices1] = getEllipsoidMesh([], 10);

rot = vrrotvec2mat(vrrotvec(normal, [0 0 1]));
faces1 = [faces1, faces1(:, 1)];

[faces1, vertices1] = getSlice(faces1, vertices1, normal, point, thickness);
vertices1 = (rot * vertices1.').';

if SHOW_MESH
    showMesh(faces1, vertices1, false);
end

%% apply slice profile

% iso2mesh function
[vertices2, elements, tmp] = surf2mesh(vertices1, faces1, ...
    min(vertices1), max(vertices1), 1, 0.000001);
elements = elements(:, 1 : 4);

imDomainBox = [min(vertices2); max(vertices2)];

intensities = assignIntensityWithMap(elements, vertices2, imDomainBox, ...
    profile);
intensities = permute(intensities, [1, 3, 2]);
[faces2, gradients] = simplifyElements(elements, vertices2, intensities);

%%
figure, trisurf(faces2, vertices2(:, 1), vertices2(:, 2), vertices2(:, 3), ...
    gradients, 'facealpha', 0.8);
axis equal
colormap gray

%% compute FT of both meshes

[Lx, Ly, Lz] = getFOV(vertices1);

kSamples = getUniformKsamples(kSampleNumX, kSampleNumY, Lx, Ly);

tic
FT1 = polyFT(faces1, vertices1, kSamples, 'Type', 'mex', 'Parallel', true, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'Verbose', 1);
t = toc;
printTimingInfo(t, size(faces1, 1), numel(kSamples) / 3);

tic
FT2 = polyFT(faces2, vertices2, kSamples, 'Type', 'mex', 'Parallel', true, ...
    'OuterLoopType', 'faces', 'FacesType', 'matrix', 'Gradients', gradients, ...
    'Verbose', 1);
t = toc;

printTimingInfo(t, size(faces2, 1), numel(kSamples) / 3);

%% reconstruct

I1 = cartesianRecon(FT1, kSampleNumX, kSampleNumY, Lx, Ly, thickness);
I2 = cartesianRecon(FT2, kSampleNumX, kSampleNumY, Lx, Ly, thickness);

if SHOW_RECON
    figure('name', 'ideal slice selection'), imshow(abs(I1(end : -1 : 1, :)))
    figure('name', 'non-ideal slice selection'), imshow(abs(I2(end : -1 : 1, :)))
end
