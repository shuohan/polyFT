% complie 

cd ./polyFT

if strcmp(computer, 'MACI64') || strncmp(computer, 'PCWIN', 5)
    mex -largeArrayDims -lmwblas calcSingleFaceFTMex.c useful_functions.c
    mex -largeArrayDims -lmwblas calcSingleKSampleFTMex.c useful_functions.c 
    mex -largeArrayDims -lmwblas prepareForSingleKSample.c useful_functions.c
elseif strcmp(computer, 'GLNXA64')
    mex -largeArrayDims -lmwblas calcSingleFaceFTMex.c useful_functions.c CC=gcc-4.7 CFLAGS="-fexceptions -fPIC -fno-omit-frame-pointer -pthread"
    mex -largeArrayDims -lmwblas calcSingleKSampleFTMex.c useful_functions.c CC=gcc-4.7 CFLAGS="-fexceptions -fPIC -fno-omit-frame-pointer -pthread"
    mex -largeArrayDims -lmwblas prepareForSingleKSample.c useful_functions.c CC=gcc-4.7 CFLAGS="-fexceptions -fPIC -fno-omit-frame-pointer -pthread"
end

cd ../utils_testing/
mex gridMex.c grid.c CFLAGS="-fexceptions -fPIC -fno-omit-frame-pointer -pthread" CC=gcc-4.7

cd ../
